# Apps

## TODO Extended

### Contribute to RAPPS

```php
<?php
interface TODO
{
  # define yourself as "CORPORATE_CONTRIBUTOR" if it's apply to
  #                  e.g. define("CORPORATE_CONTRIBUTOR", 1)

  public function __construct( $yourname, $myhability = ICouldContributWith::CODING );

  // @return bool
  public function readCodeLicense( $filename = 'LICENSE' );

  // @return bool
  public function readContributorsLicensesAgreement( $filename = 'CONTRIBUTING.md' );

  public function howCouldIContributWith( $myhability = ICouldContributWith::CODING );

  // @return bool
  public function IagreedContributorsLicensesAgreement();

  public function setProjectIssuesIWillFix($issuesCount);

}

abstract class ICouldContributWith
{
  const CODING                = 'coding Contributions';
  const CODING_BACKEND        = 'coding backend Contributions';
  const CODING_FRONTEND       = 'coding frontend Contributions';
  const CODING_JS             = 'coding javascript Contributions';
  const CODING_TEST           = 'coding test Contributions';
  const CODING_DOCUMENTATION  = 'coding documentation Contributions';
  const CODING_CARE           = 'coding care Contributions';
  const CODING_DATABASE       = 'coding database Contributions';
  const CODING_TUTORIALS      = 'coding tutorials Contributions';
  const CODING_AUTOMATION     = 'coding automation Contributions';
  const CODING_ARCHITECTURE   = 'coding architecture Contributions';
  const CODING_THEME          = 'coding theme Contributions';
  const CODING_ALL_ABOVE      = 'coding Contributions to everything';
  const CODING_ALL_ABOVE_MORE = 'coding Contributions as God';
}

abstract class ICouldDoIt implements TODO
{
  public $contributors = ["{best of us}"];
  protected $contributor = "{best of me}";
  protected $issues2fix = 0;
  protected $myhability = ICouldContributWith::CODING;

  public function __construct($yourname, $myhability=ICouldContributWith::CODING) {
    if ( $this->readCodeLicense() and
         $this->readContributorsLicensesAgreement() )
    {
      $this->contributor = $yourname;
      $this->myhability = $myhability;
    }
  }

  public function howCouldIContributWith($myhability=ICouldContributWith::CODING)
  {
    $this->myhability = $myhability?$myhability:$this->myhability;

    switch ($this->myhability) {
      case ICouldContributWith::CODING:
        do {
          $DOING = <<<'EOT'
          # do the best of my hability
          # code...
          # code...
          # test your own code...
          # fix bugs
          # add features
          # test your own code...
          # fix bugs
          # code...
          # test your own code...
          # test again your own code with other values...
          # test again your own code with other values...
EOT;
          --$this->issues2fix;
          echo "I'm doing my best! I'm $myhability with.\n".$DOING;
        } while (ICouldDoIt::todoListNotEmpty(@$TODO)>0);
        break;

      default:
        do {
          $DOING = <<<'EOT'
          # do the best of my hability
          # may be code...
          # may be code...
          # code...
          # do otherwise you want and like to contribute
EOT;
          echo "I'm doing my best, too! I'm $myhability with.\n".$DOING;
          --$this->issues2fix;
        } while (ICouldDoIt::todoListNotEmpty(@$TODO)>0);
        break;
    }

    return sprintf("I, %s, could do it by myself, couldn't I? And I will have fun!", $this->contributor);

  }

  public function IAgreeWithContributorsLicensesAgreement()
  {
    if (!$this->IagreedContributorsLicensesAgreement()) {
      $msg = sprintf("Oops! I, %s, don't like to have fun! Cheers!\n", $this->contributor);
      echo $msg;
      return $msg;
    }

    if ( $this->IAgreeWithContributorLicenseAgreement() ) {
      $this->contributors[] = $this->contributor;
    } else {
      $this->contributor = NULL;
    }

    $this->IAgreeWithIndividualContributorLicenseAgreement();

    if (defined("CORPORATE_CONTRIBUTOR")) {
      $this->IAgreeWithCorporateContributorLicenseAgreement();
    }

    return true;
  }

  protected function IAgreeWithContributorLicenseAgreement()
  {
    # see on file CONTRIBUTING.md the Contributor License Agreement
    echo "Yes! I, $this->contributor, read, accepted and agreed with Contributor License Agreement!\n";
    return true;
  }

  protected function IAgreeWithIndividualContributorLicenseAgreement()
  {
    # see on file CONTRIBUTING.md the Individual Contributor License Agreement
    echo "Yes! I, $this->contributor, read, accepted and agreed with Individual Contributor License Agreement!\n";
    return true;
  }

  protected function IAgreeWithCorporateContributorLicenseAgreement()
  {
    # see on file CONTRIBUTING.md the Corporate Contributor License Agreement
    echo "Yes! I, $this->contributor, read, accepted and agreed with Corporate Contributor License Agreement!\n";
    return true;
  }

  private function todoListNotEmpty() {
    return ICouldDoIt::getProjectIssuesContributorWillFix();
  }

  private function getProjectIssuesContributorWillFix()
  {
    return $this->issues2fix;
  }

  // @return bool
  abstract public function readCodeLicense( $filename = 'LICENSE' );

  // @return bool
  abstract public function readContributorsLicensesAgreement( $filename = 'CONTRIBUTING.md' );

  // @return bool
  abstract public function IagreedContributorsLicensesAgreement();

  abstract public function setProjectIssuesIWillFix($issuesCount);
}
```


## be a Contributor

Create your own ***```Contributor class```*** implementing the class bellow...

```php
<?php
class Contributor extends ICouldDoIt implements TODO
{

}

$I = new Contributor('{your name goes here}');

# deal yourself
$Icandomore = new Contributor('{your name goes here}', ICouldContributWith::CODING_ALL_ABOVE_MORE);
```
