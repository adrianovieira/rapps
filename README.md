# RAPPS

Aplicação base para desenvolvimento sob o framework [```Silex```](http://silex.sensiolabs.org/).

Alguns documentos:

* README.md (este arquivo)
* [LICENCE](LICENSE)
* [TODO.md](TODO.md)
* [TODO_Extended.md](TODO_Extended.md)
* [CONTRIBUTING.md](CONTRIBUTING.md)
* [doc/INSTALL.md](doc/INSTALL.md)
* [doc/PROVISION.md](doc/PROVISION.md)
* [doc/CODING.md](doc/CODING.md)