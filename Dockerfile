FROM registry.gitlab.com/adrianovieira/rapps:base

LABEL maintainer Adriano Vieira <adriano.svieira at gmail.com>

# setup apache default vhost
COPY setup/25-dev.rapps.hacklab_non-ssl.conf /etc/httpd/conf.d/25-dev.rapps.hacklab_non-ssl.conf
COPY setup/25-dev.rapps.hacklab_ssl.conf /etc/httpd/conf.d/25-dev.rapps.hacklab_ssl.conf
COPY setup/rapps_setup_sample_selfsigned_ssl_http_server.cert /etc/ssl/dev.rapps.hacklab.cert
COPY setup/rapps_setup_sample_selfsigned_ssl_http_server.key /etc/ssl/dev.rapps.hacklab.key

# database init
COPY setup/sql/ /docker-entrypoint-initdb.d/

# upload app to image
WORKDIR /var/www/html/web
ADD config /var/www/html/config
ADD src /var/www/html/src
ADD web /var/www/html/web
