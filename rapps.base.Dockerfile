FROM centos:7

LABEL maintainer Adriano Vieira <adriano.svieira at gmail.com>

# install base packs
#RUN yum -y install httpd; yum clean all;
RUN yum -y install epel-release ;  \
    yum -y install mod_ssl openssl php php-cli php-common php-xml php-ldap \
                   php-mysql php-pgsql composer; \
    yum clean all;

WORKDIR /var/www/html
ADD composer-dist.json composer.json
RUN composer install --no-dev

# Simple startup script to avoid some issues observed with container restart (CentOS tip)
ADD setup/run-apache-httpd.sh /run-apache-httpd.sh
RUN chmod -v +x /run-apache-httpd.sh

# expose apache port
ONBUILD EXPOSE 80
ONBUILD EXPOSE 443

# run rapps on apache
ONBUILD CMD ["/run-apache-httpd.sh"]
