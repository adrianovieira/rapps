<?php
/**
 * RAPPS Controller
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @license @see LICENCE
 */

//namespace Apps // ajustar nome de namespace
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Connection;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

// Accepting a JSON Request Body
// http://silex.sensiolabs.org/doc/cookbook/json_request_body.html
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

// obtem rotas (URL) definidas para a aplicação
require 'app-routes.php';

/* COMENTADA customização de erros
 * TODO: personalizar páginas de erro (envio de mensagens/email com detalhes)
 */
$app->error(function (\Exception $e, $code) use ($app) {
    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    if ($app['debug']) {
      return new Response($app['twig']->resolveTemplate($templates)->render(array('debug' => $app['debug'], 'code' => $code, 'error' => $e)), $code);
    }

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
}));
$app['twig.path'] = array(__DIR__,__DIR__.'/Rapps/views',__DIR__.'/Rapps');
$app['twig.options'] = array('cache' => $app['apps_config']['environment']['cache'].'/twig',
                             'auto_reload' => $app['debug']);

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addFunction(new \Twig_SimpleFunction('cdn_server_url', function ($asset) use ($app) {
        return sprintf(getenv('CDN_SERVER_URL')?:$app['apps_config']['environment']['cdn_server_url']);
    }));
    return $twig;
}));
// Rapps\Security: user provider
$app->register(new Silex\Provider\SecurityServiceProvider());

$app['user.provider'] =  new \Rapps\User\UserProvider($app['dbs']['write']);

$ldap_options = $app['apps_config']['ldap'];
$ldap_options['fallback_db'] = $app['apps_config']['ldap_fallback_server_down'];

// RAPPS auth: custom Authentication Provider
$app['security.authentication_listener.factory.rapps'] = $app->protect(function ($name, $options) use ($app) {
    // define the authentication provider object
    $app['security.authentication_provider.'.$name.'.rapps'] = $app->share(function () use ($app) {
        return new \Rapps\Security\AuthenticationProvider($app['security.user_provider.secured'],
                                                         $ldap_options,
                                                         $app['security.encoder_factory']);
    });
     //define the authentication listener object
    $app['security.authentication_listener.'.$name.'.rapps'] = $app->share(function () use ($app) {
    });
    return array(
        // the authentication provider id
        'security.authentication_provider.'.$name.'.rapps',
        // the authentication listener id
        'security.authentication_listener.'.$name.'.rapps',
        // the entry point id
        null,
        // the position of the listener in the stack
        'pre_auth'
    );
});
$app['security.authentication_manager'] = new \Rapps\Security\AuthenticationProvider($app['user.provider'],
                                                                                    $ldap_options,
                                                                                    $app['security.encoder_factory']);
// end RAPPS auth

// rules: ROLE_USER, ROLE_ADMIN
$app['security.access_rules'] = array(
    array('^/admin', 'IS_AUTHENTICATED_FULLY'),
);
$app['security.access_rules'] = array( # URL /admin or /setup needs ROLE_ADMIN
    array('^/admin', 'ROLE_ADMIN'),
    array('^/setup', 'ROLE_ADMIN'),
    array('^/', 'IS_AUTHENTICATED_ANONYMOUSLY'),
    array('^.*$', 'ROLE_USER'),
);
$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_USER'),
);

$app['security.firewalls'] = array(
    'login' => array(
      'pattern' => '^/login$',
    ),
    'secured' => array(
      'pattern' => '^/',
      'anonymous' => true,
      'form' => array('login_path' => '/login',
                      'check_path' => '/admin/login_check',
                      'default_target_path'=> '/',
                      'always_use_default_target_path'=>true,
                    ),
      'logout' => array('logout_path' => '/admin/logout',  // url to call for logging out
                        'invalidate_session' => true),
      'users' => $app->share(function() use ($app) {
                          return $app['user.provider'];
                        }
      ),
    ),
);

$app['security.authentication.success_handler.secured'] = $app->share(function() use ($app) {
  return new Rapps\Security\LoginAuthenticationSuccessHandler($app['security.http_utils'], array(), $app);
});

// dados simples para informações e ajuda (footer)
$app['info_data'] = Rapps\Admin\CommonController::obtemAPPInfo();

// dados simples para informações (header)
$app['user_nickname'] = '('.$app['session']->get('auth_user').')';
$app['user_name'] = 'Nome do Usuário';
$app['user_perfil'] = 'Perfil do Usuário';
$app['user_lotacao'] = 'Lotação do Usuário';

$app['user_logged'] = $app['session']->get('user_logged');
$app['session']->get('cdUsuario');

$app->boot();
