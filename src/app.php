<?php
/**
 * RAPPS base
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @license @see LICENCE
 * @see README.md
 * @see doc/INSTALL.md
 * @see doc/PROVISION.md
 * @see TODO.md
 */

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Application\SecurityTrait;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Silex\Provider\MonologServiceProvider;
use Symfony\Component\Yaml\Yaml;

$app = new Application();
// obtem parâmetros base da RAPPS (backend)
$app['apps_config'] = array();
if (file_exists(__DIR__."/../config/config.dist.yaml")) {
  $app['apps_config'] = Yaml::parse(file_get_contents(__DIR__."/../config/config.dist.yaml"));
} else {die('FATAL ERROR: RAPPS Distributed configuration file not found');}

if (file_exists(__DIR__."/../config/config.local.yaml")) {
  $apps_config_local = Yaml::parse(file_get_contents(__DIR__."/../config/config.local.yaml"));
  $app['apps_config'] = array_merge($app['apps_config'], $apps_config_local);
}
// enable the debug mode
$app['debug'] = !$app['apps_config']['environment']['production'];

if (!$app['apps_config']['environment']['production']) {
  $app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => $app['apps_config']['environment']['cache'].'/log/development.log',
  ));
}

// database
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
      'write' => array(
        'charset' => $app['apps_config']['database']['charset'],
        'driver' => $app['apps_config']['database']['driver'],
        'host' => $app['apps_config']['database']['write']['server'],
        'port' => array_key_exists('port',$app['apps_config']['database']['write'])?$app['apps_config']['database']['write']['port']:null,
        'dbname' => $app['apps_config']['database']['schema'],
        'user' => $app['apps_config']['database']['username'],
        'password' => array_key_exists('password',$app['apps_config']['database'])?$app['apps_config']['database']['password']:null,
      ),
      'read' => array(
        'charset' => $app['apps_config']['database']['charset'],
        'driver' => $app['apps_config']['database']['driver'],
        'host' => $app['apps_config']['database']['read']['server'],
        'port' => array_key_exists('port',$app['apps_config']['database']['read'])?$app['apps_config']['database']['read']['port']:null,
        'dbname' => $app['apps_config']['database']['schema'],
        'user' => $app['apps_config']['database']['username'],
        'password' => array_key_exists('password',$app['apps_config']['database'])?$app['apps_config']['database']['password']:null,
      )
    ),
));

// obtem parâmetros base da RAPPS (negócio)
$stmt = $app['dbs']['read']->executeQuery('SELECT * FROM setup');
$app['apps_config'] = array_merge($app['apps_config'], $stmt->fetch());


date_default_timezone_set($app['apps_config']['environment']['date_default_timezone']);

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.domains' => array(),
    'locale' => $app['apps_config']['i18n_locale'],
    'fallback_locale' => $app['apps_config']['i18n_fallback'],
));
$app->register(new SessionServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());

return $app;
