<?php
/**
 * Programa principal para a aplicação Apps
 *
 * Metodos específicos de uso geral
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Admin
 * @license @see LICENCE
 */

namespace Rapps\Admin;

use Silex\Application;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CommonController {

  public static function obtemAPPInfo() {
    // dados simples para informações e ajuda (footer)
    $filename = __DIR__."/../../../".'VERSION'; //na raiz RAPPS
    $version_number = "0.0.1-devel (unreleased)";
    $version_data_atualizacao = '(undefined)';
    if (file_exists($filename)) {
      $handle = fopen( $filename, "r");
      $content = trim(fgets($handle));
      fclose($handle);
      if (!empty($content)) {
        $version_number = $content;
        $version_data_atualizacao = strftime('%x %X',filemtime($filename));
      }
    }
    //$browser = get_browser(null, true);
    if (empty($browser))
      $browser = $_SERVER['HTTP_USER_AGENT'];

    $app_info_build = array('version' => $version_number,
                            'build' => $version_data_atualizacao,
                            'environment' =>array(
                                'server' => $_SERVER['SERVER_NAME'],
                                'my_ip' => $_SERVER['REMOTE_ADDR'],
                                'browser' => $browser
                              )
                        );
    return $app_info_build;
  } // end obtemAPPInfo

}
