<?php
/**
 * Programa principal para a aplicação Apps
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Admin
 * @license @see LICENCE
 */

namespace Rapps\Admin;

use Silex\Application;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class IndexAdminController {
  public function indexAction(Request $request, Application $app) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    return $app['twig']->render('Admin/views/rapps-admin.twig');
  }
}
