<?php
/**
 * Programa principal para a aplicação Apps
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @license @see LICENCE
 */

namespace Rapps\Admin;

use Silex\Application;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class IndexController {
  // lista de aplicativos disponíveis e autorizados (para o usuário)
  private $_lista_aplicativos;

  public function indexAction(Request $request, Application $app) {

    $aplicativos = new AdminAplicativosController($app['dbs']['write']);
    $_lista_aplicativos = $aplicativos->getApplications();
    $_array_merge = false;

    // OBTEM aplicativos guest
    if ($aplicativos->loadApplications4Guest()) {
      $_lista_aplicativos = $aplicativos->getApplications();
      $_array_merge = true;
    }

    $token = $app['security.token_storage']->getToken();

    if (null !== $token) {
        $user = $token->getUser();
        if ($app['security.authorization_checker']->isGranted('ROLE_USER')) {
          if ($aplicativos->loadApplicationsByUsername($user->getUsername(), $user->getRoles())) {
            $_aplicativos = $aplicativos->getApplications();
            if ($_array_merge) {
              $_lista_aplicativos['aplicativos'] = array_merge($_lista_aplicativos['aplicativos'], $_aplicativos['aplicativos']);
            } else {
              $_lista_aplicativos = $_aplicativos;
            }
            $_array_merge = true;
          }

          /*if (condition) {
            // OBTEM aplicativos por grupo do usuario
            //!$aplicativos->loadApplicationsByGroupid('group_id', 'group_rules')
            //array_merge($array1, $array2)
          }*/
        }
    }

    $this->_lista_aplicativos = $_lista_aplicativos;

    return $app['twig']->render('views/rapps-painel.twig', $this->_lista_aplicativos);
  }
}
