<?php
namespace Rapps\Admin;

use Silex\Application;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class IndexAdminSetupController {
  public function indexAction(Request $request, Application $app) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $sent = false;
    //inicializa valores do formulário
    $default=array();

    $form = $app['form.factory']->createBuilder('form', $default)
      ->add('username_minlen', 'integer', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
              'label' => 'Número mínimo de caracteres para nome de usuário:',
              'attr' => array('class' => 'form-control',
                              'placeholder' => '',
                              'ng-model' => 'username_minlen.value',
                              #'ng-init'=>"username_minlen='".@$default['username_minlen']."'",
                              #'maxlength' => '3',
                              'ng-minlength' => '1',
                              'ng-maxlength' => '2',
                              #'ng-pattern' => '[0-9]',
                              'ng-class' => '{alert: form.form[username_minlen].$valid}',
                              'ng-trim' => 'true',
                              'min'=>"3",
                              'max'=>"50",
                            )
      ))
      ->add('password_minlen', 'integer', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
              'label' => 'Número mínimo de caracteres para senha de usuário:',
              'attr' => array('class' => 'form-control',
                              'placeholder' => '',
                              'ng-model' => 'password_minlen',
                              #'ng-init'=>"password_minlen='".@$default['password_minlen']."'",
                              'maxlength' => '2',
                              'min'=>"3",
                              'max'=>"25",
                              'ng-minlength' => '1',
                              'ng-maxlength' => '2',
                              #'ng-pattern' => '[0-9]',
                              'ng-class' => '{alert: form.form[password_minlen].$valid}',
                              'ng-trim' => 'true'
                            ),
              'label_attr' => array(
                  'class' => 'control-label'
              )
      ))
      ->add('ldap_fallback_server_down', 'choice', array(
              'choices' => array(1 => 'Sim', 2 => 'Não'),
              'expanded' => true,
              'label' => 'Permite fallback em falha em Autenticação LDAP (servidor indisponível):',
              'attr' => array(#'class' => 'form-control',
                              #'placeholder' => '',
                              'ng-model' => 'ldap_fallback_server_down.choice',
                              #'ng-init'=>"ldap_fallback_server_down='".@$default['ldap_fallback_server_down']."'",
                              'ng-class' => '{alert: form.form[ldap_fallback_server_down].$valid}',
                            )
      ))
      ->add('submit', 'submit', array(
        'label' => 'Enviar',
        'attr' => array('class' => 'btn btn-default',
                        'ng-disabled' => '!form.$valid')
      ))
      /*->add('reset', 'reset', array(
        'label' => 'Limpar',
        'attr' => array('class' => 'btn btn-default')
      ))*/
      ->getForm();

    $form->handleRequest($request);

    if($form->isValid()) {
      $data = $form->getData();

      $sent = true;
    }

    return $app['twig']->render('Admin/views/rapps-admin-setup-form.twig',
                                array('form' => $form->createView(),
                                'sent' => $sent));

  }
}
