<?php
/**
 * Controller para a app Admin
 *
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Admin
 * @license @see LICENCE
 */
namespace Rapps\Admin;

use Silex\Application;
use Doctrine\DBAL\Connection;
use Symfony\Component\Security\Acl\Exception\Exception;

class AdminAplicativosController
{
    private $conn;
    private $_lista_app = array( 'aplicativos' => array(
        array(
          'app_id' => null,
          'is_active' => true,
          'auth' => true,
          'logotype' => null,
          'version' => null,
          'route' => null,
          'rule' => 'ROLE_USER',
          'name' => 'Aplicativos ainda em construção',
          'title' => 'Sem acesso a aplicativos ativos'
        )
      )
    );

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function loadApplicationsByUsername($username, $user_roles)
    {
      try {
        $stmt = $this->conn->executeQuery('SELECT * FROM users_has_applications uha
                                                    JOIN applications ON uha.app_id = applications.app_id
                                                    WHERE uha.username = ?', array(strtolower($username)));
        if (!$applications = $stmt->fetchAll()) {
            throw new Exception(sprintf('Username "%s" does not has applications.', $username));
        }

        $this->_lista_app = array( 'aplicativos' => $applications);
        $result = true;
      } catch (Exception $e) {
        //throw new Exception(sprintf('Username "%s" does not exist.', $username));
        $result = false;
      }

      return $result;

    }

    public function loadApplicationsByGroupid($group_id, $group_roles)
    {
      try {
        $stmt = $this->conn->executeQuery('SELECT * FROM groups_has_applications gha
                                                    JOIN applications ON gha.app_id = applications.app_id
                                                    WHERE uha.group_id = ?', array(strtolower($group_id)));
        if (!$applications = $stmt->fetch()) {
            throw new Exception(sprintf('Group "%s" does not has applications.', $group_id));
        }

        $this->_lista_app = array( 'aplicativos' => $applications);
        $result = true;
      } catch (Exception $e) {
        //throw new Exception(sprintf('Username "%s" does not exist.', $username));
        $result = false;
      }

      return $result;

    }

    public function loadApplications4Guest()
    {
      try {
        $stmt = $this->conn->executeQuery('SELECT * FROM applications
                                                    WHERE is_active AND roles = ?', array('ROLE_GUEST'));
        if (!$applications = $stmt->fetchAll()) {
            throw new Exception(sprintf('Guest does not has applications.'));
        }

        $this->_lista_app = array( 'aplicativos' => $applications);
        $result = true;
      } catch (Exception $e) {
        //throw new Exception(sprintf('Username "%s" does not exist.', $username));
        $result = false;
      }

      return $result;

    }

    // Obtêm lista de APP da RAPPS
    public function getApplications() {

      return $this->_lista_app;
    }
}
