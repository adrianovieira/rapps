<?php
/**
 * Classe para gestão de usuários
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage User
 * @license @see LICENCE
 */

namespace Rapps\User;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class User implements AdvancedUserInterface, EquatableInterface
{
  private $username;
  private $password;
  private $salt;
  private $roles;
  private $isActive;
  private $user_expired;
  private $credentials_expired;
  private $is_locked;
  private $displayname;

  public function __construct($username, $password, array $roles, $isActive,
                              $user_expired, $credentials_expired, $is_locked,
                              $displayname, $auth_method)
  {
      $this->username = $username;
      $this->password = $password;
      $this->roles = $roles;
      $this->isActive = $isActive;
      $this->user_expired = $user_expired;
      $this->credentials_expired = $credentials_expired;
      $this->is_locked = $is_locked;
      $this->displayname = $displayname;
      $this->auth_method = $auth_method;
  }

  public function getRoles()
  {
      return $this->roles;
  }

  public function getPassword()
  {
      return $this->password;
  }

  public function getSalt()
  {
      return $this->salt;
  }

  public function getUsername()
  {
      return $this->username;
  }

  public function eraseCredentials()
  {
  }

  public function isEqualTo(UserInterface $user)
  {
      if (!$user instanceof UserExtended) {
          return false;
      }

      if ($this->password !== $user->getPassword()) {
          return false;
      }

      if ($this->salt !== $user->getSalt()) {
          return false;
      }

      if ($this->username !== $user->getUsername()) {
          return false;
      }

      return true;
  }

  public function isAccountNonExpired()
  {
    return $this->user_expired;
  }

  public function isAccountNonLocked()
  {
    return $this->is_locked;
  }

  public function isCredentialsNonExpired()
  {
    return $this->credentials_expired;
  }

  public function isEnabled()
  {
    return $this->isActive;
  }

  public function getDisplayname()
  {
      return $this->displayname;
  }

  public function getAuthenticationMethod()
  {
      return $this->auth_method;
  }

}
