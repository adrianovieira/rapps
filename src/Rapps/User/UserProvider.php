<?php
/**
 * Classe para gestão de usuários
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage User
 * @license @see LICENCE
 */

namespace Rapps\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\DBAL\Connection;

class UserProvider implements UserProviderInterface
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function loadUserByUsername($username)
    {
      try {
        $stmt = $this->conn->executeQuery('SELECT * FROM users
                                                    LEFT JOIN users_has_applications uha ON users.username = uha.username
                                                    WHERE users.username = ?', array(strtolower($username)));
        if (!$user = $stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
      } catch (\Exception $e) {
        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
      }

      $user_expired = false;
      if(null !== $user['user_expire']) {
        if($user['user_expire'] !== date("0000-00-00 00:00:00")) {
          $user_expired = $user['user_expire'] < date("Y-m-d H:i:s");
        }
      }

      $credentials_expired = false;
      if(null !== $user['credentials_expire']) {
        if($user['credentials_expire'] !== date("0000-00-00 00:00:00")) {
          $credentials_expired = $user['credentials_expire'] < date("Y-m-d H:i:s");
        }
      }

      return new User($user['username'], $user['password'], explode(',', $user['roles']), $user['is_enabled'],
                      !$user_expired, !$credentials_expired,
                      !($user['is_locked']), $user['name'], $user['auth_method']);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }

    public function updateLdapCredentials($credentials)
    {
      try {
        $count = $this->conn->executeUpdate('UPDATE users SET password = ? WHERE users.username = ?', $credentials);

      } catch (\Exception $e) {
        return false;
      }

      return true;
    }

    public function updateUserLdapData($account_data, $username)
    {
      try {
        $account_data['username'] = $username;
        $count = $this->conn->executeUpdate('UPDATE users SET name = :cn, email = :mail, is_enabled = :accountstatus WHERE users.username = :username', $account_data);
      } catch (\Exception $e) {
        // logging (sprintf("[LDAP] User DB update error. UnsupportedUserException: (%s)", $e->getMessage()));
        return false;
      }

      return true;
    }

    public function updateUserLastLogin($username, $account_data)
    {
      try {
        $account_data['username'] = $username;
        $count = $this->conn->executeUpdate('UPDATE users SET last_login = :last_login WHERE users.username = :username', $account_data);
      } catch (\Exception $e) {
        return false;
      }

      return true;
    }

}
