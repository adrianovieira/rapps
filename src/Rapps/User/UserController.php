<?php
/**
 * Controller para a aplicação Users
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage base
 * @license @see LICENCE
 */
namespace Rapps\User;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\DBAL\DBALException;

class UserController implements ControllerProviderInterface {

  public function connect(Application $app)
  {
      // cria novo módulo controller
      $controllers = $app['controllers_factory'];

      // lista serviços disponíveis
      $controllers->get('/', 'Rapps\User\UserController::index' )->bind('users');
      $controllers->get('/dashboard', 'Rapps\User\UserController::getUser' );
      $controllers->get('/{username}', 'Rapps\User\UserController::getUser' );
      $controllers->put('/enable/{username}/{is_enabled}', 'Rapps\User\UserController::enableUserById');
      $controllers->post('/', 'Rapps\User\UserController::addUser');
      $controllers->put('/{username}', 'Rapps\User\UserController::updateUser');
      $controllers->delete('/{username}', 'Rapps\User\UserController::deleteUser');

      return $controllers;
  }

  public function index(Request $request, Application $app, $username='') {

    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $user_main_vars = array('app_name' => "Usuários",
                            'app_title'=> "Gerenciamento de usuários");

    return $app['twig']->render('Rapps/User/views/User-main.twig', $user_main_vars);
  } // end index

  public function getUser(Request $request, Application $app, $username='') {

    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    // obtem dados para edição da aplicação cadastrada
    /*
    id, username, name, auth_method, email,  is_enabled, is_locked, user_expire, credentials_expire, last_login, roles, time_created, app_id_default

    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(60) NOT NULL,
    `name` VARCHAR(100) NOT NULL DEFAULT '',
    `password` VARCHAR(255) NOT NULL DEFAULT '',
    `auth_method` INT NULL,
    `email` VARCHAR(100) NULL DEFAULT NULL,
    `is_enabled` TINYINT(1) NULL DEFAULT '1',
    `is_locked` TINYINT(1) NULL DEFAULT NULL,
    `user_expire` TIMESTAMP NULL DEFAULT NULL,
    `credentials_expire` TIMESTAMP NULL DEFAULT NULL,
    `last_login` TIMESTAMP NULL DEFAULT NULL,
    `salt` VARCHAR(255) NOT NULL DEFAULT '',
    `roles` VARCHAR(255) NOT NULL DEFAULT '',
    `time_created` INT(11) UNSIGNED NOT NULL DEFAULT '0',
    `isEnabled` TINYINT(1) NOT NULL DEFAULT '1',
    `confirmationToken` VARCHAR(100) NULL DEFAULT NULL,
    `timePasswordResetRequested` INT(11) UNSIGNED NULL DEFAULT NULL,
    `app_id_default` VARCHAR(45) NULL,
    */
    $username = $app->escape($username);
    if ($username) {
      $statement = $app['dbs']['read']->prepare("SELECT id, username, name, auth_method, email,  is_enabled, is_locked,
                                        user_expire, credentials_expire, last_login, roles, time_created, app_id_default
                                                 FROM users
                                                 WHERE username = '$username'");
      $statement->execute();
      $users = $statement->fetch();
    } else {
      $statement = $app['dbs']['read']->prepare("SELECT id, username, name, auth_method, email,  is_enabled, is_locked,
                                        user_expire, credentials_expire, last_login, roles, time_created, app_id_default
                                                 FROM users");
      $statement->execute();
      $users = $statement->fetchAll();
    }

    return $app->json($users, 202);
  } // end getUser

  // habilitar/desabilitar usuário cadastrada
  public function enableUserById(Request $request, Application $app, $username='', $is_enabled=0) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $username = $app->escape($username);
      $habilitar = $app->escape($is_enabled);
      $app_data = $app['dbs']['write']->update('users', array('is_enabled' => $habilitar), array('username' => $username));
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("USER ID (%s) %s com sucesso",$username,($habilitar?'habilitado':'desabilitado'));
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("USER ID (%s) não %s - ocorreu erro",$username,($habilitar?'habilitado':'desabilitado'));
      $response['message']['debug'] = sprintf("USER ID (%s) não %s [%s|%s]",$username,($habilitar?'habilitado':'desabilitado'), $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end enableUserById

  public function addUser(Request $request, Application $app) {
    // continua se autenticado, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAuthenticated($app);

    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $data['username'] = $app->escape($request->request->get('username'));
    $data['name'] = $app->escape($request->request->get('name'));
    $data['email'] = $app->escape($request->request->get('email'));
    $data['auth_method'] = $app->escape($request->request->get('auth_method'));
    $data['is_enabled'] = 0;
    $data['password'] = \Rapps\Security\SecurityController::encodePassword($app, strtolower($data['username']));
    $response_status = 202;
    try {
      $statement = $app['dbs']['write']->insert("users", $data);
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("USER ID (%s) cadastrado com sucesso",$data['username']);
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("USER ID (%s) não cadastrado",$data['username']);
      $response['message']['debug'] = sprintf("USER ID (%s) não cadastrado [%s|%s]",$data['username'], $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  }  // end addUser

  public function updateUser(Request $request, Application $app, $username) {
    // continua se autenticado, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAuthenticated($app);

    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $username = $app->escape($username);

    $data['username'] = $app->escape($request->request->get('username'));
    $data['name'] = $app->escape($request->request->get('name'));
    $data['email'] = $app->escape($request->request->get('email'));
    $data['auth_method'] = $app->escape($request->request->get('auth_method'));
    $response_status = 202;
    try {
      $statement = $app['dbs']['write']->update('users', array('name' => $data['name'],
                                                            'username' => $data['username'],
                                                            'email' => $data['email'],
                                                            'auth_method' => $data['auth_method']),
                                                      array('username' => $username));
      if ($statement) {
        $response_status = 202;
        $response['message']['success'] = true;
        $response['message']['text'] = sprintf("USER ID (%s) atualizado com sucesso",$data['username']);
      } else {
        $response_status = 500;
        $response['message']['success'] = false;
        $response['message']['text'] = sprintf("USER ID (%s) não atualizado",$data['username']);
      }
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("USER ID (%s) não atualizado",$data['username']);
      $response['message']['debug'] = sprintf("USER ID (%s) não atualizado [%s|%s]",$data['username'], $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end updateUser

  // excluir usuário cadastrado
  public function deleteUser(Request $request, Application $app, $username) {
    // continua se autenticado, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAuthenticated($app);

    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $username = $app->escape($username);
      $app_data = $app['dbs']['write']->delete('users', array('username' => $username));
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("USER ID (%s) excluído com sucesso",$username);
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("USER ID (%s) não excluído - ocorreu erro",$username);
      $response['message']['debug'] = sprintf("USER ID (%s) não excluído [%s|%s]",$username, $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end deleteUser


}
