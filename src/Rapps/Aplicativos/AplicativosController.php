<?php
/**
 * Programa principal para a administrar aplicativos
 *
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage AdminAplicativos
 * @license @see LICENCE
 */

namespace Rapps\Aplicativos;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\DBAL\DBALException;

class AplicativosController implements ControllerProviderInterface {

  public function connect(Application $app)
  {
      // cria novo módulo controller
      $controllers = $app['controllers_factory'];


      /*
       * admin Aplicativos rest api   (um lembrete)
       * get: ./aplicativos           (lista)
       * get: ./aplicativos/{id}      (obtem 1 registro)
       * post: ./aplicativos          (cria/adiciona)
       * put: ./aplicativos/{id}      (atualiza)
       * delete: ./aplicativos/{id}   (exclui)
       */

      // lista serviços disponíveis
      $controllers->get('/', 'Rapps\Aplicativos\AplicativosController::index' )->bind('aplicativos');
      $controllers->get('/lista', 'Rapps\Aplicativos\AplicativosController::getAppsDataLista');
      $controllers->get('/{app_id}', 'Rapps\Aplicativos\AplicativosController::getAppsDataById');
      $controllers->get('/{app_id}/users', 'Rapps\Aplicativos\AplicativosController::getAppsUsersById');
      $controllers->put('/habilitar/{app_id}/{is_active}', 'Rapps\Aplicativos\AplicativosController::habilitarAppsById');
      $controllers->put('/publicar/{app_id}/{roles}', 'Rapps\Aplicativos\AplicativosController::publicarAppsById');
      $controllers->delete('/{app_id}', 'Rapps\Aplicativos\AplicativosController::deleteAppsDataById');
      $controllers->put('/{app_id}', 'Rapps\Aplicativos\AplicativosController::updateAppsDataById');
      $controllers->post('/', 'Rapps\Aplicativos\AplicativosController::addAppsData');

      return $controllers;
  }

  public function index(Request $request, Application $app) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $sent = false;
    //inicializa valores do formulário
    $default=array();

    $form = $app['form.factory']->createBuilder('form', $default)
      ->add('app_id', 'text', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5))),
              'label' => 'Identificador',
              'attr' => array('class' => 'form-control',
                              'placeholder' => 'identificador da aplicação (exemplo: admin)',
                              'title' => 'Insira no mínimo 5 (cinco) caracteres',
                              'ng-model' => 'adminAplicativos.aplicativo.data.app_id',
                              'ng-init'=>"adminAplicativos.aplicativo.data.app_id='".@$default['app_id']."'",
                              'maxlength' => '25',
                              'ng-minlength' => '5',
                              'ng-maxlength' => '25',
                              #'ng-pattern' => '[a-zA-Z]',
                              'ng-class' => '{alert: form.form[app_id].$valid}',
                              'ng-trim' => 'true',
                              'ng-readonly' => 'adminAplicativos.aplicativo.readonly_id'
                            )
      ))
      ->add('route', 'text', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5))),
              'label' => 'URL de acesso',
              'attr' => array('class' => 'form-control',
                              'placeholder' => 'URL para acesso à aplicação (exemplo: /admin/painel)',
                              'title' => 'Insira no mínimo 5 (cinco) caracteres',
                              'ng-model' => 'adminAplicativos.aplicativo.data.route',
                              'ng-init'=>"adminAplicativos.aplicativo.data.route='".@$default['route']."'",
                              'ng-minlength' => '5',
                              #'ng-pattern' => '[a-zA-Z]',
                              'ng-class' => '{alert: form.form[route].$valid}',
                              'ng-trim' => 'true'
                            )
      ))
      ->add('name', 'text', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 15))),
              'label' => 'Nome',
              'attr' => array('class' => 'form-control',
                              'placeholder' => 'Nome da aplicação (exemplo: Configurações da RAPPS)',
                              'title' => 'Insira no mínimo 15 (quinze) caracteres',
                              'ng-model' => 'adminAplicativos.aplicativo.data.name',
                              'ng-init'=>"adminAplicativos.aplicativo.data.name='".@$default['name']."'",
                              'maxlength' => '30',
                              'ng-minlength' => '15',
                              #'ng-pattern' => '[a-zA-Z]',
                              'ng-class' => '{alert: form.form[name].$valid}',
                              'ng-trim' => 'true'
                            )
      ))
      ->add('title', 'text', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 20))),
              'label' => 'Título',
              'attr' => array('class' => 'form-control',
                              'placeholder' => 'Título da aplicação (exemplo: Paniel de configuração da RAPPS)',
                              'title' => 'Insira no mínimo 20 (vinte) caracteres',
                              'ng-model' => 'adminAplicativos.aplicativo.data.title',
                              'ng-init'=>"adminAplicativos.aplicativo.data.title='".@$default['title']."'",
                              'ng-minlength' => '20',
                              #'ng-pattern' => '[a-zA-Z]',
                              'ng-class' => '{alert: form.form[title].$valid}',
                              'ng-trim' => 'true'
                            )
      ))
      ->add('logotype', 'text', array(
              'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 20))),
              'label' => 'Logotipo',
              'attr' => array('class' => 'form-control',
                              'placeholder' => 'Font Awesome icone logotipo da aplicação (exemplo: fa-wrench)',
                              'title' => 'Insira no mínimo 5 (vinte) caracteres',
                              'ng-model' => 'adminAplicativos.aplicativo.data.logotype',
                              'ng-init'=>"adminAplicativos.aplicativo.data.logotype='".@$default['logotype']."'",
                              'ng-minlength' => '5',
                              #'ng-pattern' => '[a-zA-Z]',
                              'ng-class' => '{alert: form.form[title].$valid}',
                              'ng-trim' => 'true'
                            )
      ))
      ->getForm();

    $form->handleRequest($request);

    return $app['twig']->render('Rapps/Aplicativos/views/aplicativos-main.twig',
                                array('form' => $form->createView(),
                                      'sent' => $sent,
                                      'display' => true,
                                      'applications' => $aplicativos,
                                      'message_success' => $_message_success,
                                      'message_error' => $_message_error,
                                      'upload_max_filesize' => ini_get('upload_max_filesize'),
                                      )
                              );

  } // end indexAction

  // obtem dados para edição da aplicação cadastrada
  public function getAppsDataById(Request $request, Application $app, $app_id='') {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    // obtem dados para edição da aplicação cadastrada
    $app_id = $app->escape($app_id);
    $statement = $app['dbs']['read']->prepare("SELECT * FROM applications WHERE app_id = '$app_id'");
    $statement->execute();
    $aplicativo = $statement->fetch();

    return $app->json($aplicativo, 202);
  } // end getAppsDataById

  // atualiza dados de aplicação cadastrada
  public function updateAppsDataById(Request $request, Application $app, $app_id='') {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    // obtem dados de atualização da aplicação cadastrada
    $app_id = $app->escape($app_id);
    $data['app_id'] = $app->escape($request->request->get('app_id'));
    $data['route'] = $app->escape($request->request->get('route'));
    $data['name'] = $app->escape($request->request->get('name'));
    $data['title'] = $app->escape($request->request->get('title'));
    $data['logotype'] = $app->escape($request->request->get('logotype'));
    $response_status = 202;
    try {
      $statement = $app['dbs']['write']->update('applications', array('name' => $data['name'],
                                                            'title' => $data['title'],
                                                            'route' => $data['route'],
                                                            'logotype' => $data['logotype']),
                                                      array('app_id' => $data['app_id']));;
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) atualizada com sucesso",$data['app_id']);

    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      //$response['message']['text'] = sprintf("APPS ID (%s) não atualizada [%s|%s]",$data['app_id']);
      $response['message']['debug'] = sprintf("APPS ID (%s) não atualizada [%s|%s]",$data['app_id'], $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end updateAppsDataById

  // adiciona aplicação
  public function addAppsData(Request $request, Application $app) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $data['app_id'] = $app->escape($request->request->get('app_id'));
    $data['route'] = $app->escape($request->request->get('route'));
    $data['name'] = $app->escape($request->request->get('name'));
    $data['title'] = $app->escape($request->request->get('title'));
    $response_status = 202;
    try {
      $statement = $app['dbs']['write']->insert("applications", $data);
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) cadastrada com sucesso",$data['app_id']);
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("APPS ID (%s) não cadastrada",$data['app_id']);
      $response['message']['debug'] = sprintf("APPS ID (%s) não cadastrada [%s|%s]",$data['app_id'], $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end addAppsData

  // obtem dados para listagem das aplicações cadastradas
  public function getAppsDataLista(Request $request, Application $app) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $statement = $app['dbs']['read']->prepare("SELECT * FROM applications");
    $statement->execute();
    $aplicativos = $statement->fetchAll();

    return $app->json($aplicativos, 202);
  } // end getAppsDataLista

  // exclui aplicação cadastrada
  public function deleteAppsDataById(Request $request, Application $app, $app_id='') {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $app_id = $app->escape($app_id);
      $app_data = $app['dbs']['write']->delete('applications', array('app_id' => $app_id));
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) excluído com sucesso",$app_id);
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("APPS ID (%s) não excluído - ocorreu erro",$app_id);
      $response['message']['debug'] = sprintf("APPS ID (%s) não excluído [%s|%s]",$app_id, $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end deleteAppsDataById

  // habilitar/desabilitar aplicação cadastrada
  public function habilitarAppsById(Request $request, Application $app, $app_id='', $is_active=0) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $app_id = $app->escape($app_id);
      $habilitar = $app->escape($is_active);
      $app_data = $app['dbs']['write']->update('applications', array('is_active' => $habilitar), array('app_id' => $app_id));
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) %s com sucesso",$app_id,($habilitar?'habilitado':'desabilitado'));
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("APPS ID (%s) não %s - ocorreu erro",$app_id,($habilitar?'habilitado':'desabilitado'));
      $response['message']['debug'] = sprintf("APPS ID (%s) não %s [%s|%s]",$app_id,($habilitar?'habilitado':'desabilitado'), $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end habilitarAppsById

  // habilitar/desabilitar acesso anonimo/restrito à aplicação cadastrada
  public function publicarAppsById(Request $request, Application $app, $app_id='', $roles=0) {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $app_id = $app->escape($app_id);
      $habilitar = $app->escape($roles);
      $app_data = $app['dbs']['write']->update('applications', array('roles' => ($habilitar?'ROLE_GUEST':0)), array('app_id' => $app_id));
      $response['message']['publicar'] = $habilitar;
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) %s acesso público",$app_id,($habilitar?'habilitado':'desabilitado'));
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("APPS ID (%s) não %s - ocorreu erro",$app_id,($habilitar?'habilitado':'desabilitado'));
      $response['message']['debug'] = sprintf("APPS ID (%s) não %s [%s|%s]",$app_id,($habilitar?'habilitado':'desabilitado'), $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end publicarAppsById

  // obtem usuários da aplicação cadastrada
  public function getAppsUsersById(Request $request, Application $app, $app_id='') {
    // continua se autenticado como Admin, senão: throw AccessDeniedHttpException
    \Rapps\Security\SecurityController::isAdmin($app);

    $response_status = 202;
    try {
      $app_id = $app->escape($app_id);
      //$stmt = $this->conn->executeQuery('SELECT * FROM applications
      //                                            LEFT JOIN users_has_applications uha ON uha.app_id = applications.app_id
      //                                            WHERE applications.app_id = ?', array(strtolower($app_id)));

      $statement = $app['dbs']['read']->prepare('SELECT applications.app_id,users.username,users.name FROM applications
                                                  LEFT JOIN users_has_applications uha ON applications.app_id = uha.app_id
                                                  LEFT JOIN users ON uha.username = users.username
                                                  WHERE applications.app_id = :app_id');
      $statement->bindValue('app_id', strtolower($app_id));
      $statement->execute();
      $users = $statement->fetchAll();
      $response['users'] = $users;
      $response['message']['success'] = true;
      $response['message']['text'] = sprintf("APPS ID (%s) obtidos com sucesso",$app_id);
    } catch (\Exception $e) {
      $response_status = 500;
      $response['message']['success'] = false;
      $response['message']['text'] = sprintf("APPS ID (%s) não obtido - ocorreu erro",$app_id);
      $response['message']['debug'] = sprintf("APPS ID (%s) não obtido [%s|%s]",$app_id, $e->getCode(),$e->getMessage());
    }

    return $app->json($response, $response_status);
  } // end getAppsUsersById

}
