<?php
/**
 * This file is part of the Apps package.
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Security
 * @license @see LICENCE
 *
 */

namespace Rapps\Security;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

define('RAPPS_AUTH_METHOD_DB', 0); # database authentication
define('RAPPS_AUTH_METHOD_LDAP', 1); # LDAP authentication
define('RAPPS_AUTH_METHOD_API_TOKEN', 2); # REST/API token authentication

class AuthenticationProvider implements AuthenticationProviderInterface {

  private $userProvider;
  private $ldap_options;
  private $encoderFactory;

  public function __construct(UserProviderInterface $userProvider, $ldap_options, $encoderFactory)
  {
    $this->userProvider = $userProvider;
    $this->ldap_options = $ldap_options;
    $this->encoderFactory = $encoderFactory;
  }

  public function authenticate(TokenInterface $token)
  {
    /**
     * TODO:
     *    - logging ocorrências em autenticação (válidas e inválidas)
     */
    $user = $this->userProvider->loadUserByUsername($token->getUsername());
    $username = $token->getUsername();

    if ($user->getAuthenticationMethod() === NULL) {
      throw new AuthenticationException('User authentication method invalid.');
    }

    $this->checkAuthenticationStatus($user, $token);

    switch ($user->getAuthenticationMethod()) {
      case RAPPS_AUTH_METHOD_LDAP:
        try {

          $ldap = new LdapBindAuthenticationProvider($user, $token, $this->ldap_options);
          $ldap->checkAuthentication($user, $token);
          # if LDAP authentication don't fail update DB user data (for fallback)
          $this->userProvider->updateLdapCredentials(array($this->encoderFactory->getEncoder($user)->encodePassword($token->getCredentials()),$token->getUsername()));
          $this->userProvider->updateUserLdapData($ldap->loadUser($token->getUsername()), $token->getUsername());
        } catch (\Exception $e) {
          if ( !$ldap->canFallbackDB() ) {
            throw new AuthenticationException($e->getMessage());
          }

          if ($e instanceof AuthenticationServiceException) {
            # if LDAP authentication fail try DB authentication (fallback)
            $this->checkDBAuthentication($user, $token);
          } else {
            throw new AuthenticationException($e->getMessage());
          }
        }
        break;

      case RAPPS_AUTH_METHOD_DB:
        $this->checkDBAuthentication($user, $token);
        break;

      default:
        throw new AuthenticationException('The RAPPS authentication method invalid.');
        break;
    }

    try {
      $authenticatedToken = new UsernamePasswordToken($user, $token->getCredentials(), 'APPS_PK', $user->getRoles());
      $authenticatedToken->setUser($user);
    } catch (\Exception $e) {
      throw new AuthenticationException('The RAPPS authentication failed.');
    }

    // update timestamp of user last login
    $this->userProvider->updateUserLastLogin($token->getUsername(), array('last_login' => date('Y-m-d H:i:s')));

    return $authenticatedToken;
  }

  public function supports(TokenInterface $token)
  {
    return $token instanceof Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
  }

  // método adaptado de DaoAuthenticationProvider
  protected function checkDBAuthentication(UserInterface $user, UsernamePasswordToken $token)
  {
      $currentUser = $token->getUser();
      if ($currentUser instanceof UserInterface) {
          if ($currentUser->getPassword() !== $user->getPassword()) {
              throw new BadCredentialsException('The credentials were changed from another session.');
          }
      } else {
          if ('' === ($presentedPassword = $token->getCredentials())) {
              throw new BadCredentialsException('The presented password cannot be empty.');
          }

          if (!$this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $presentedPassword, $user->getSalt())) {
              throw new BadCredentialsException('The presented password is invalid.');
          }
      }
  }

  // método adaptado de DaoAuthenticationProvider
  protected function retrieveUser($username, UsernamePasswordToken $token)
  {
      $user = $token->getUser();
      if ($user instanceof UserInterface) {
          return $user;
      }

      try {
          $user = $this->userProvider->loadUserByUsername($username);

          if (!$user instanceof UserInterface) {
              throw new AuthenticationServiceException('The user provider must return a UserInterface object.');
          }

          return $user;
      } catch (UsernameNotFoundException $e) {
          $e->setUsername($username);
          throw $e;
      } catch (\Exception $e) {
          $e = new AuthenticationServiceException($e->getMessage(), 0, $e);
          $e->setToken($token);
          throw $e;
      }
  }

  protected function checkAuthenticationStatus(UserInterface $user, UsernamePasswordToken $token)
  {
    if (!($user->isEnabled())) {
      $ex = new DisabledException('User account is disabled.');
      $ex->setUser($user);
      throw $ex;
    }

    if (!($user->isAccountNonLocked())) {
      $ex = new LockedException('User account is locked.');
      $ex->setUser($user);
      throw $ex;
    }

    if (!($user->isAccountNonExpired())) {
      $ex = new AccountExpiredException('User account has expired.');
      $ex->setUser($user);
      throw $ex;
    }

    if (!($user->isCredentialsNonExpired())) {
      $ex = new CredentialsExpiredException('User credentials have expired.');
      $ex->setUser($user);
      throw $ex;
    }
  }

} # end AuthenticationProvider
