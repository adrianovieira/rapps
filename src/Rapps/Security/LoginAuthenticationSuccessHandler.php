<?php
/**
 *  Login Authentication Handler
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Security
 * @license @see LICENCE
 */

namespace Rapps\Security;

use Silex\Provider\SessionServiceProvider;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Silex\Application;

class LoginAuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
  protected $app = null;

  public function __construct(HttpUtils $httpUtils, array $options, Application $app)
  {
    parent::__construct($httpUtils, $options);
    $this->app = $app;
  }

  public function encodePassword($password) {
    return $this->app['security.encoder.digest']->encodePassword($password, '');
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token)
  {
    /*
    // RAPPS legada
    $legacy['data'] = array(
                     //[logado] => true
                    'logado' => $_SESSION['logado'],
                    //[mensagem] =>
                    'mensagem' => $_SESSION['mensagem'],
                    //[idFuncionario] => 250362
                    'idFuncionario' => $_SESSION['idFuncionario'],
                    //[estUser] => DF
                    'estUser' => $_SESSION['estUser'],
                    //[nmUsuario] => Adriano dos Santos Vieira - DATAPREVDF
                    'nmUsuario' => $_SESSION['nmUsuario'],
                    //[telefone] => +556132073378
                    'telefone' => $_SESSION['telefone'],
                    //[ou] => uid=adriano.vieira,ou=SUAS,ou=DIT,ou=DATAPREV,dc=gov,dc=br
                    'ou' => $_SESSION['ou'],
                    //[unidade] => N/A
                    'unidade' => $_SESSION['unidade'],
                    //[email] => adriano.vieira@dataprev.gov.br
                    'email' => $_SESSION['email'],
                    //[entidade] =>
                    'entidade' => $_SESSION['entidade'],
                    //[auth_user] => adriano.vieira
                    'auth_user' => $_SESSION['auth_user'],
                    //[auth_pass] => 54tNvkCgHC3wlSqcOnapbQZ_MqMBEhkGygWfPU9O8xA
                    'auth_pass' => $_SESSION['auth_pass'],
                    //[senha_sistema] => 8YfW4HYa-AmOVmj41UH3kO0TpgzN12xHFPdhih3s20k
                    'senha_sistema' => $_SESSION['senha_sistema'],
                    //[start_page] =>
                    'start_page' => $_SESSION['start_page'],
                    //[usu_nick] => adriano_vieira
                    'usu_nick' => $_SESSION['usu_nick'],
                    //[nome] => Adriano dos Santos Vieira
                    'nome' => $_SESSION['nome']
                );*/

    //[cdUsuario] => 341568
    $this->app['session']->set('auth_user',$token->getUser()->getUsername());
    $this->app['session']->set('handler_success', 'valeu silex');
    $this->app['session']->set('user_logged', true);
    $this->app['session']->set('user', array('username' => $token->getUser()));

    $user = $token->getUser();

    return $this->httpUtils->createRedirectResponse($request, $this->determineTargetUrl($request));
  }
}
