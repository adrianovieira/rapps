<?php
/**
 * Package Security para a aplicação Apps
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Security
 * @license @see LICENCE
 */

namespace Rapps\Security;

use Silex\Application;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/*
 * TODO:
 * - Implementar como um serviço para a RAPPS
 * - realizar logging de acesso não autorizado (USER, IP) e DDOS
 */
class SecurityController {
  public function __construct(Application $app)
  {
      $this->app = $app;
  }

  public function isAuthenticated(Application $app)
  {
    // check security: is authenticated
    if (!$app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')) {
      throw new AccessDeniedException('Unable to access this page!');
    }
  } // end isAuthenticated

  public function isAdmin(Application $app)
  {
    // check security: is authenticated as Admin
    if (!$app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
      if ($app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')) {
        $username = $app['security.token_storage']->getToken()->getUser()->getUsername();
      }
      throw new AccessDeniedException($username.', unable to access this page!');
    }
  } // end isAdmin

  public function encodePassword(Application $app, $password)
  {
    // security: encode password
    return $app['security.authentication.success_handler.secured']->encodePassword($password);
  } // end encodePassword

} // end SecurityController
