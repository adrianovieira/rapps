<?php
/**
 * Classe para gestão de usuários
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage User
 * @license @see LICENCE
 */

namespace Rapps\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zend\Ldap\Exception\LdapException;
use Zend\Ldap;

class LdapBindAuthenticationProvider
{
  private $ldap_conn;
  private $ldap_host;
  private $ldap_port = null;
  private $ldap_basedn;
  private $ldap_bind_rdn;
  private $ldap_uid_key = 'uid';
  private $ldap_attributes = array("ou", "sn", "givenname", "mail", "cn", "accountstatus", "displayName");
  private $account_data = null;
  private $ldap_fallback_db = null;

  public function __construct(UserInterface $user, UsernamePasswordToken $token, $ldap_options)
  {
    $this->ldap_host = $ldap_options['server'];
    $this->ldap_port = @$ldap_options['port'] ? $ldap_options['port'] : null;
    $this->ldap_basedn = $ldap_options['bind_dn'];
    $this->ldap_bind_rdn = $ldap_options['bind_rdn'];
    $this->ldap_fallback_db = $ldap_options['fallback_db']?$ldap_options['fallback_db']:false;
    $options = array(
        'host'              => $this->ldap_host,
        #'username'          => 'CN=user1,DC=foo,DC=net', # autenticação para conectar (usário)
        #'password'          => 'pass1', # autenticação para conectar (senha)
        'bindRequiresDn'    => true,
        'baseDn'            => $this->ldap_basedn,
    );
    try {
      $this->ldap_conn = new \Zend\Ldap\Ldap($options);

    } catch (LdapException $e) {
      if ($e->getCode() === LdapException::LDAP_X_EXTENSION_NOT_LOADED) {
        $runtime_exception_message = '';
        // logging (sprintf("[LDAP] RuntimeException: Instale php-ldap ou reinicie o serviço HTTP. (%s)",
        //                                            $e->getMessage()));
         throw new RuntimeException(sprintf("LDAP service unavailable (RuntimeException)."));
       }
       // logging (sprintf("[LDAP] RuntimeException: (%s)", $e->getMessage()));
       throw new LdapException(sprintf('LDAP service unavailable (RuntimeException).'));
    }

  }

  public function loadUser($username=null)
  {
    if ($username) {
      $accountname = $this->ldap_conn->getCanonicalAccountName($username, \Zend\Ldap\Ldap::ACCTNAME_FORM_DN);
      $accountdata = $this->ldap_conn->getEntry($accountname,$this->ldap_attributes);
      $result = array();
      foreach ($accountdata as $key => $value) {
        $result[$key] = array_key_exists(0, $value)?$value[0]:$value;
      }
      $this->account_data = $result;
      $this->account_data['accountstatus'] = $accountdata['accountstatus'][0]=='active'?true:false;
    }

    return $this->account_data;
  }

  public function canFallbackDB()
  {
    return $this->ldap_fallback_db;
  }

  public function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
  {
    try {
      $result = $this->ldap_conn->bind($token->getUsername(), $token->getCredentials());
    } catch (\Exception $e) {
      $result = $this->ldap_conn->disconnect();

      if ($e->getCode() === LdapException::LDAP_OPERATIONS_ERROR) {
        // check operation error
        try {
          $raw_ldap_resource = $this->ldap_conn->getResource();
        } catch (\Exception $e) {
          if ($e->getCode() === LdapException::LDAP_SERVER_DOWN) {
            // logging (sprintf("[LDAP] LDAP service unavailable. RuntimeException: (%s)", $e->getMessage()));
            throw new AuthenticationServiceException(sprintf('LDAP service unavailable.'));
          }
        }
        // logging (sprintf("[LDAP] LDAP operation error. RuntimeException: (%s - %s)", $e->getCode(), $e->getMessage()));
        throw new \Exception(sprintf('LDAP operation error. [%s]', $e->getMessage() ));
      }

      if ($e->getCode() === LdapException::LDAP_NO_SUCH_OBJECT) {
        // logging (sprintf("[LDAP] LDAP User does not exist. UsernameNotFoundException: (%s - %s - %s)", $token->getUsername(), $e->getCode(), $e->getMessage()));
      }
      // logging (sprintf("[LDAP] RuntimeException: (%s)", $e->getMessage()));
      throw new \Exception(sprintf('LDAP credentials is invalid.'));
    }

    try {
      $accountname = $this->ldap_conn->getCanonicalAccountName($token->getUsername(), \Zend\Ldap\Ldap::ACCTNAME_FORM_DN);
    } catch (\Exception $e) {
      $result = $e->getCode();
      $result = $this->ldap_conn->disconnect();
      // logging (sprintf("[LDAP] LDAP user does not exist. UsernameNotFoundException: (%s)", $token->getUsername()));
      throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $token->getUsername()));
    }

    try {
      $accountdata = $this->ldap_conn->getEntry($accountname,$this->ldap_attributes);
    } catch (\Exception $e) {
      $result = $this->ldap_conn->disconnect();
      // logging (sprintf("[LDAP] LDAP user data is invalid. UsernameNotFoundException: (%s)", $token->getUsername()));
      throw new UsernameNotFoundException('LDAP user data is invalid.');
    }

    if ( !($accountdata['accountstatus'][0]=='active') ) {
      // logging (sprintf("[LDAP] LDAP User is disabled. AccessDeniedException: (%s)", $token->getUsername()));
      throw new AccessDeniedException(sprintf('LDAP user is disabled.'));
    }

    # update account_data
    $result = array();
    foreach ($accountdata as $key => $value) {
      $result[$key] = array_key_exists(0, $value)?$value[0]:$value;
    }
    $this->account_data = $result;
    $this->account_data['accountstatus'] = true;

  }
}
