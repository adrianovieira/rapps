<?php
/**
 * Login Controller
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @subpackage Security
 * @license @see LICENCE
 */

namespace Rapps\Security;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class LoginController {
  public function login(Request $request, Application $app) {
    return $app['twig']->render('Security/views/login.twig', array(
          'error'         => $app['security.last_error']($request),
          'last_username' => $app['session']->get('_security.last_username'),
    ));
  }
}
