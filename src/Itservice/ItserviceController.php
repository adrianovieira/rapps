<?php
/**
 * Controller para a aplicação ITServices
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package ITService
 * @subpackage base
 * @license @see LICENCE
 */
namespace Itservice;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ItserviceController implements ControllerProviderInterface {

  public function connect(Application $app)
  {
      // cria novo módulo controller
      $controllers = $app['controllers_factory'];

      // lista serviços disponíveis
      $controllers->get('/', 'Itservice\ItserviceController::index' )->bind('itservice');
      /*
      $controllers->get('/{service_id}', 'Itservice\ItserviceController::getITServiceById' );
      $controllers->put('/{service_id}', 'Itservice\ItserviceController::updateITServiceById');
      $controllers->post('/', 'Itservice\ItserviceController::addITService');
      $controllers->delete('/{service_id}', 'Itservice\ItserviceController::deleteITServiceById');
      */

      return $controllers;
  }

  public function index(Request $request, Application $app) {

    return $app['twig']->render('Itservice/views/Itservice-main.twig');
  }

  /*
  public function addITService(Request $request, Application $app) {
    // continua se autenticado, senão: throw AccessDeniedHttpException
    \Apps\Security\SecurityController::isAuthenticated($app);

    return $app['twig']->render('Itservice/views/itservice-main.twig');
  }
  */

}
