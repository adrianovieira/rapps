<?php
/**
 * Arquivo de rotas para a aplicação Apps
 *
 * Serão registradas aqui todas as rotas a serem usadas pelas aplicações
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Rapps
 * @license @see LICENCE
 */
use Symfony\Component\HttpFoundation\Response;

$app->get('/', 'Rapps\Admin\IndexController::indexAction' )
    ->bind('homepage');

$app->get('/admin', 'Rapps\Admin\IndexAdminController::indexAction' )
    ->bind('admin');

$app->mount('/admin/aplicativos', new Rapps\Aplicativos\AplicativosController());

$app->get('/admin/setup', 'Rapps\Admin\IndexAdminSetupController::indexAction' )
    ->bind('setup');

$app->get('/login', 'Rapps\Security\LoginController::login' )->bind('login');
$app->match('/logout', function() use ($app) {
            $app['session']->clear();
            return $app->redirect($app['url_generator']->generate('homepage'));
        })->bind('logout');

/**
 * Adicone outras rotas a partir daqui
 * TODO automatizar a leitura/geração de rotas, principalmente para aplicações cadastradas
 */
$app->mount('/admin/users', new Rapps\User\UserController());
$app->mount('/itservice', new Itservice\ItserviceController());
