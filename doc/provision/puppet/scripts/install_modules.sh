# instala os módulos base e suas dependencias
# observação: r10k não instala dependências
# execute:
#   1o) r10k puppetfile install -v
# ou
#   2o) sh install_modules.sh
#
error=0
for mod in `cat puppetlabs_modules |cut -d\' -f2`;
do
    #echo -e "Instalando [\033[31m$mod\033[0m]... ";
    mod_name=`echo $mod | awk -F"@" '{ print $1 }'`;
    mod_version=`echo $mod | awk -F"@" '{ if($2) print "--version " $2 }'`;
    echo -e "Instalando [\033[32m$mod ($mod_name | $mod_version) \033[0m]... ";
    puppet module install $mod_name $mod_version --modulepath ../modules;
    #puppet module upgrade $mod --modulepath ../modules  ;
    if [ $? != 0 ]; then
      error=1
    fi
done

exit $error
