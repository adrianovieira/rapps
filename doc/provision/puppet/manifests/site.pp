# Site.pp para testes de módulos com o vagrant.
#

# para omitir mensagem de aviso de valor padrão para
# pacotes virtuais no tipo "Package"
if versioncmp($::puppetversion,'3.6.1') >= 0 {
  Package {
    allow_virtual => hiera('allow_virtual_packages',true),
  }
}

# TODO:
#  - migrar todas essas classes RAPPS em módulo puppet
#  - pós migração alterar "/vagrant/downloads" ou "/vagrant/rapps" para puppet: "files"
class rapps::params
{
  # check platform support
  case $::operatingsystem {
    'RedHat', 'CEntOs': { # operating system 'RedHat', 'CentOS'
      if versioncmp($::operatingsystemmajrelease, '7') < 0 {
        fail("RAPPS: operating system version ${::operatingsystem}-${::operatingsystemmajrelease} is not supported. Use ${::operatingsystem}>=6")
      }
    }
    'Debian_not_yet_supported_or_tested': { # operating system Debian like
      if versioncmp($::operatingsystemmajrelease, '8') < 0 {
        fail("RAPPS: operating system version ${::operatingsystem}-${::operatingsystemmajrelease} is not supported. Use ${::operatingsystem}>=8")
      }
    }
    default: {
      fail("RAPPS: operating system ${::operatingsystem} is not supported")
    }
  }

  # packages to install
  $packages = $operatingsystem ? {
    /(?i-mx:debian)/               => [ "php",
                                        "php-cli",
                                        "php-common",
                                        "php-xml",
                                      ],
    /(?i-mx:centos|fedora|redhat)/ => [ "php",
                                        "php-cli",
                                        "php-ldap",
                                        "php-common",
                                        "php-xml",
                                        "composer",
                                        "nodejs-grunt-cli",
                                        "nodejs",
                                        "npm",
                                        "git",
                                      ],
  }

  # http server rapps directory owner
  $http_owner  = $::osfamily ? {
                    /(?i-mx:debian)/  => 'www-data',
                    /(?i-mx:redhat)/  => 'apache',
                }

  # http server rapps directory group
  $http_group  = $::osfamily ? {
                    /(?i-mx:debian)/  => 'www-data',
                    /(?i-mx:redhat)/  => 'apache',
                }

  # para acesso a aplicacao, ex: http://dev.rapps.puppet
  $rapps_http_service_name = hiera('rapps_http_service_name','dev.rapps.puppet')
  # alternativas para acesso a aplicacao, ex: dev.rapps, dev.rapps.intranet
  $rapps_http_service_name_aliases = hiera('rapps_http_service_name_aliases', 'dev.rapps')
  # path de implantação, ex: /var/www/html/rapps
  $rapps_path_deploy = hiera('rapps_path_deploy', '/var/www/html/rapps')
  # url de repositório publico para obter a aplicação,
  #   ex: https://gitlab.com/adrianovieira/rapps.git
  #       file:///Users//Code/rapps/ (or http/s, ssh)
  $rapps_git_repository = hiera('rapps_git_repository', 'https://gitlab.com/adrianovieira/rapps.git')
  # versão/branch a obter do repositório da aplicação, ex: v0.0.1
  $rapps_git_repository_version = hiera('rapps_git_repository_version', 'master')

  # driver de host servidor DB para rapps, ex: mysql|postgresql
  $rapps_db_driver = hiera('rapps_db_driver' , 'mysql')
  # charset para banco de dados: utf8|latin1|...
  $rapps_db_charset = hiera('rapps_db_charset' , 'utf8')
  # nome de host servidor DB para rapps, ex: localhost
  $rapps_db_host = hiera('rapps_db_host' , 'localhost')
  # porta de host servidor DB para rapps, ex: 3306
  $rapps_db_host_port = hiera('rapps_db_host_port' , undef)
  # nome de BD no host servidor DB para rapps, ex: db_rapps
  $rapps_db_name = hiera('rapps_db_name', 'db_rapps')
  # nome de usuário para rapps conectar ao BD, ex: root
  $rapps_db_user_name = hiera('rapps_db_user_name', 'root')
  # senha de usuário para rapps conectar ao BD, ex: secret
  $rapps_db_user_pass = hiera('rapps_db_user_pass', undef)

  # production = True: implantacao em ambiente produtivo
  $rapps_env_production = hiera('rapps_env_production', false)
  # ambiente = Produção|Homologação|Teste|Desenvolvimento: tipo de ambiente produtivo
  $rapps_env_type = hiera('rapps_env_type', 'Desenvolvimento')
  # encoding para obter json, html etc
  $rapps_env_code_page = hiera('rapps_env_code_page', 'UTF-8')
  # Default timezone (see http://php.net/manual/en/timezones.php)
  $rapps_env_date_default_timezone = hiera('rapps_env_date_default_timezone', 'America/Sao_Paulo')
  # local para armazenar arquivos para melhora de desempenho e logs
  $rapps_env_cache = hiera('rapps_env_cache', '/var/tmp/rapps')

  # LDAP servidor do serviço de autenticação e autorização, ex: undef|ldap.intranet
  $rapps_ldap_server = hiera('rapps_ldap_server', undef)
  # porta no servidor do serviço de autenticação e autorização, ex: 389|636
  $rapps_ldap_server_port = hiera('rapps_ldap_server_port', undef)
  # DN no servidor do serviço de autenticação e autorização
  $rapps_ldap_bind_dn = hiera('rapps_ldap_bind_dn', "dc=gov, dc=br")
  # RDN no servidor do serviço de autenticação e autorização
  $rapps_ldap_bind_rdn = hiera('rapps_ldap_bind_rdn', "dc=gov, dc=br")
  # usuário no servidor do serviço de autenticação e autorização
  $rapps_ldap_user = hiera('rapps_ldap_user', undef)
  # senha do usuário de autenticação no serviço de autenticação e autorização
  $rapps_ldap_user_pass = hiera('rapps_ldap_user_pass', undef)
} # end rapps::params

class rapps::config (
  $rapps_env_production = $rapps::params::rapps_env_production,
  $rapps_env_type = $rapps::params::rapps_env_type,
  $rapps_env_code_page = $rapps::params::rapps_env_code_page,
  $rapps_env_date_default_timezone = $rapps::params::rapps_env_date_default_timezone,
  $rapps_env_cache = $rapps::params::rapps_env_cache,
  $http_owner = $rapps::params::http_owner, # http server rapps directory owner
  $http_group = $rapps::params::http_group, # http server rapps directory group
  $rapps_http_service_name = $rapps::params::rapps_http_service_name, # dev.rapps.puppet
  $rapps_http_service_name_aliases = $rapps::params::rapps_http_service_name_aliases, # dev.rapps
  $rapps_path_deploy = $rapps::params::rapps_path_deploy, # /var/www/html/rapps # path de implantação
  $rapps_git_repository = $rapps::params::rapps_git_repository,
  $rapps_git_repository_version = $rapps::params::rapps_git_repository_version,
  $rapps_db_driver = $rapps::params::rapps_db_driver,
  $rapps_db_charset = $rapps::params::rapps_db_charset,
  $rapps_db_host = $rapps::params::rapps_db_host,
  $rapps_db_host_port = $rapps::params::rapps_db_host_port,
  $rapps_db_name = $rapps::params::rapps_db_name,
  $rapps_db_user_name = $rapps::params::rapps_db_user_name,
  $rapps_db_user_pass = $rapps::params::rapps_db_user_pass,
  $rapps_ldap_server = $rapps::params::rapps_ldap_server,
  $rapps_ldap_server_port = $rapps::params::rapps_ldap_server_port,
  $rapps_ldap_bind_dn = $rapps::params::rapps_ldap_bind_dn,
  $rapps_ldap_bind_rdn = $rapps::params::rapps_ldap_bind_rdn,
  $rapps_ldap_user = $rapps::params::rapps_ldap_user,
  $rapps_ldap_user_pass = $rapps::params::rapps_ldap_user_pass,

  # The base URL of the npm package registry (custom facter)
  #   Default: https://registry.npmjs.org/
  #   local: http://npm.local:7001/
  $npm_config_registry = $::exec_env_npm_config_registry,

  # The bower registry URL (custom facter)
  #   Default: https://bower.herokuapp.com
  #   local: http://bower.local:9990/
  $bower_registry = $::exec_env_bower_registry,

  ) inherits rapps::params
{
  package { $packages: ensure => installed }

  # PDO driver de host servidor DB para rapps, ex: mysql|postgresql
  $rapps_db_pdo_driver  = $rapps::params::rapps_db_driver ? {
                    /(?i-mx:mysql|mariadb)/  => 'pdo_mysql',
                    /(?i-mx:postgresql_not_supported)/  => 'pdo_pgsql', # not tested
                    default  => undef,
                }
  if (undef == $rapps_db_pdo_driver) {
    fail("RAPPS: database ${rapps::params::rapps_db_driver} is not supported")
  }
  notice("PDO Driver ${rapps_db_pdo_driver}")

  $rapps_config_inline_template = @(END)
# Arquivo de configuração padrão de backend para a aplicação
# se for o caso, para evitar mostrar no browser/navegador, parametrize o http-server com "*.yaml Deny from all"
#
# Crie o arquivo "config.local.yaml" a partir deste; para manter os dados específicos necessários
environment:
  production: <%= @rapps_env_production %> # production = True: implantacao em ambiente produtivo
  type: <%= @rapps_env_type %> # ambiente = Produção|Homologação|Teste|Desenvolvimento: tipo de ambiente produtivo
  code_page: <%= @rapps_env_code_page %> # encoding para obter json
  date_default_timezone: <%= @rapps_env_date_default_timezone %> # Default timezone (see http://php.net/manual/en/timezones.php)
  cache: <%= @rapps_env_cache %> # local para armazenar arquivos para melhora de desempenho e logs

database:
  driver: <%= @rapps_db_pdo_driver %> # driver do tipo de banco de dados: pdo_mysql|pdo_pgsql
  schema: <%= @rapps_db_name %> #nome do banco de dados (schema) a ser usado
  charset: <%= @rapps_db_charset %> #charset para banco de dados: utf8|latin1|...
  username: <%= @rapps_db_user_name %> # usuário do banco de dados (schema)
  #password: secret # senha do usuário do banco de dados (schema)
  <% if @rapps_db_user_pass -%>password: <%= @rapps_db_user_pass %><% end %>
  write:
    server: <%= @rapps_db_host %> # servidor de hospedagem do banco de dados
    #port = 3306 #porta de acesso no servidor de hospedagem do banco de dados
    <% if @rapps_db_host_port -%>port: <%= @rapps_db_host_port %><% end %>
  read:
    server: <%= @rapps_db_host %> # servidor de hospedagem do banco de dados
    #port = 3306 #porta de acesso no servidor de hospedagem do banco de dados
    <% if @rapps_db_host_port -%>port: <%= @rapps_db_host_port %><% end %>

<% if @rapps_ldap_server -%>
ldap:
  server: <%= @rapps_ldap_server %> # servidor do serviço de autenticação e autorização
  #port: 389 # porta no servidor do serviço de autenticação e autorização, ex: 389|636
  <% if @rapps_ldap_server_port -%>port: <%= @rapps_ldap_server_port %><% end %>
  bind_dn: <%= @rapps_ldap_bind_dn %> # DN no servidor do serviço de autenticação e autorização
  bind_rdn: <%= @rapps_ldap_bind_rdn %> # RDN no servidor do serviço de autenticação e autorização
  #username: ldap_user # usuário no servidor do serviço de autenticação e autorização
  <% if @rapps_ldap_user -%>username: <%= @rapps_ldap_user %><% end %>
  #password: secret # senha do usuário de autenticação no serviço de autenticação e autorização
  <% if @rapps_ldap_user_pass -%>password: <%= @rapps_ldap_user_pass %><% end %>
<% end -%>

debug: # para uso futuro; ainda não implementado (por ora se "production: false", realiza logging)
  logging: False # logging = True: para depuracao em desenvolvimento
  level: 0 # level: se DEBUG, nivel de debug desejado (0=Warning, 1=Error etc)
  END

  #
  /* proxy settings
  *   create facters: http_proxy and/or https_proxy
  *      'http_proxy' => "http://localhost:5865",
  *      'https_proxy' => "http://localhost:5865",
  */
  if ($http_proxy or $https_proxy) {
    if ($http_proxy) {
      $http_proxy = ["http_proxy=${http_proxy}"]
    }
    if ($https_proxy) {
      $https_proxy = ["https_proxy=${https_proxy}"]
    }
    $proxy = $http_proxy + $https_proxy
    warning("RAPPS: proxy settings ${proxy}")
  }

  # exec_environment segundo registry para NPM e BOWER registry + proxy
  if $npm_config_registry {
      $exec_environment_npm = ["npm_config_registry=${npm_config_registry}"]
  } else { $exec_environment_npm = $proxy }

  if $bower_registry {
      $exec_environment_bower = ["bower_registry=${bower_registry}"]
  } else { $exec_environment_bower = $proxy }

  if ($proxy) {
    $exec_environment = ["COMPOSER_HOME=/var/tmp/composer"] + $proxy
  } else {
    $exec_environment = ["COMPOSER_HOME=/var/tmp/composer"]
  }

  warning("RAPPS: exec environment ${exec_environment}")

} # end rapps::config

class rapps::web () inherits rapps
{
  class { 'apache': }
  class { 'apache::mod::php': }
  class { 'mysql::bindings':
     php_enable => true,
     notify  => Service['httpd'],
  }

  if ($vagrant_redirect_port != undef) {
    info("Configura redirecionamento de porta HTTP para porta Vagrant ${vagrant_redirect_port}")
  }
  apache::vhost { "${rapps::config::rapps_http_service_name} non-ssl":
    servername => "${rapps::config::rapps_http_service_name}",
    serveraliases => "$rapps::config::rapps_http_service_name_aliases",
    port => '80',
    docroot => "${rapps::config::rapps_path_deploy}/web",
    redirect_status => 'permanent',
    redirect_dest   => "https://${rapps::config::rapps_http_service_name}${vagrant_redirect_port}",
    directories  => [
      { path           => "${rapps::config::rapps_path_deploy}/web",
        allow_override => ['All'], #['AuthConfig', 'Indexes'],
      },
    ],
  }

} # end rapps::web

class rapps::web::ssl () inherits rapps::web
{
  file { 'ssl certificate cert file':
    path => "/etc/ssl/${rapps::config::rapps_http_service_name}.cert",
    source => "/vagrant/downloads/rapps_setup_sample_selfsigned_ssl_http_server.cert",
  }
  file { 'ssl certificate key file':
    path => "/etc/ssl/${rapps::config::rapps_http_service_name}.key",
    source => "/vagrant/downloads/rapps_setup_sample_selfsigned_ssl_http_server.key",
  }

  apache::vhost { "${rapps::config::rapps_http_service_name} ssl":
    servername => "${rapps::config::rapps_http_service_name}",
    serveraliases => "$rapps::config::rapps_http_service_name_aliases",
    port => '443',
    docroot => "${rapps::config::rapps_path_deploy}/web",
    directories  => [
      { path           => "${rapps::config::rapps_path_deploy}/web",
        allow_override => ['All'], #['AuthConfig', 'Indexes'],
      },
    ],
    ssl      => true,
    ssl_cert => "/etc/ssl/${rapps::config::rapps_http_service_name}.cert",
    ssl_key  => "/etc/ssl/${rapps::config::rapps_http_service_name}.key",
  }

} # end rapps::web::ssl

class rapps::web::vendor () inherits rapps
{
  file { 'rapps_composer.json':
    path => "${rapps_path_deploy}/composer.json",
    source => "/vagrant/rapps/composer-dist.json",
  }

  exec {'get_vendor_lib':
    path => '/bin',
    command => 'composer install',
    cwd => $rapps::config::rapps_path_deploy,
    onlyif => ["test ! -f ${rapps::config::rapps_path_deploy}/vendor/autoload.php"],
    environment => $exec_environment,
  }
} # end rapps::web::vendor

class rapps::web::cli () inherits rapps
{
  warning("RAPPS: exec environment to npm and bower ${exec_environment_npm}/${exec_environment_bower}")
  exec {'get_cli_dev_req':
    path => '/bin',
    command => 'npm install',
    cwd => "${rapps::config::rapps_path_deploy}/scripts",
    onlyif => [ "test ! -d ${rapps::config::rapps_path_deploy}/web/vendor",
                "test ! -d ${rapps::config::rapps_path_deploy}/web/vendor/angularjs"],
    environment => $exec_environment_npm,
  }->
  exec {'get_cli_lib':
    path => '/bin',
    command => 'grunt',
    cwd => "${rapps::config::rapps_path_deploy}/scripts",
    onlyif => ["test ! -d ${rapps::config::rapps_path_deploy}/web/vendor/angularjs"],
    environment => $exec_environment_bower,
  }
} # end rapps::web::cli

class rapps::web::deploy () inherits rapps
{
  exec { 'rapps_deploy':
    path => '/usr/bin',
    command => "git clone --recursive -b ${rapps_git_repository_version} ${rapps_git_repository} ${rapps_path_deploy}",
    onlyif => ["test ! -d ${rapps_path_deploy}/.git"],
    environment => $exec_environment,
  }

  file { 'rapps_path_deploy':
    path => "${rapps_path_deploy}",
    ensure => directory,
    owner => $http_owner,
    group => $http_group,
    require => Exec['rapps_deploy'],
  }

  file { 'rapps_path_web':
    path => "${rapps::config::rapps_path_deploy}/web",
    ensure => directory,
    owner => $http_owner,
    group => $http_group,
    require => Exec['rapps_deploy'],
  }

  file { 'rapps_path_var':
    path => "${rapps_path_deploy}/var",
    ensure => directory,
    owner => $http_owner,
    group => $http_group,
    require => Exec['rapps_deploy'],
  }

  file { 'rapps_path_var_logs':
    path => "${rapps_path_deploy}/var/logs",
    ensure => directory,
    owner => $http_owner,
    group => $http_group,
    require => Exec['rapps_deploy'],
  }

  file { 'rapps_config.local':
    path => "${rapps_path_deploy}/config/config.local.yaml",
    content => inline_template($rapps_config_inline_template),
    require => Exec['rapps_deploy'],
  }

} # end rapps::web::deploy

class rapps::db () inherits rapps
{
  class { '::mysql::server': }
  mysql::db { "${rapps_db_name}":
    user     => "${rapps_db_user_name}",
    password => "${rapps_db_user_pass}",
    host     => "${$rapps_db_host}",
    grant    => ['ALL'],
    sql      => ['/vagrant/rapps/setup/sql/rapps_setup_000-standard-schema_dev_0.0.1-unreleased.sql',
                 '/vagrant/rapps/setup/sql/rapps_setup_801-standard-data_dev_0.0.1-unreleased.sql'],
    import_timeout => 900,
  }

} # end rapps::db

class rapps () inherits rapps::config
{
  notice("Ambiente: [${environment}] em ${::osfamily}/${::operatingsystem}")
} # end rapps

# == Class: rapps::test
# realizar teste de integração ou demonstração
class rapps::test {
  class { 'rapps::web::ssl': }
  ->
  class { 'rapps::web::deploy': }
  ->
  class { 'rapps::web::cli': }
  ->
  class { 'rapps::web::vendor': }

  class { 'rapps::db': }
} # end rapps::test

# == Class: rapps::devel
# Reliazr contribuições no desenvolvimento da aplicação
class rapps::devel {
  class { 'rapps::config':
    rapps_path_deploy => '/vagrant/rapps',
   }
   ->
   class { 'rapps::web::ssl': }
   ->
   class { 'rapps::web::cli': }
   ->
   class { 'rapps::web::vendor': }

   class { 'rapps::db': }
} # rapps::devel

# == Class: os_base::firewall
# Configura firewall para uso da aplicação
class os_base::firewall {
  include firewall
  firewall { "000 accept all icmp requests":
    proto  => "icmp",
    action => "accept",
  }->
  firewall { '22 allow ssh access':
    dport   => 22,
    proto  => tcp,
    action => accept,
  }->
  firewall { '100 allow http and https access':
    dport   => [80, 443],
    proto  => tcp,
    action => accept,
  }->
  firewall { "999 drop all other requests":
    action => "drop",
  }
} # os_base::firewall

# == Class: os_base::proxy
# Configura proxy para implementar a aplicação
/* proxy settings
 *   create facters: http_proxy and/or https_proxy
 *      $http_proxy = "http://localhost:5865"
 *      $https_proxy = "http://localhost:5865"
 *
 * localproxy settings
 * parametrize os valores necessários
 *      $localproxy = false|true
 * e heira local (environment/hieradata/local_hiera.yaml)
 * exemplo:
    ---
    proxy_digest_user: <proxy_username>
    proxy_digest_password: <prox_userpass>
 */
class os_base::proxy {
  if ($http_proxy or $https_proxy) {
    if ('RedHat' == $::osfamily) {
      file { 'yum.conf':
        path => "/etc/yum.conf",
        source =>'/vagrant/downloads/yum.conf.proxy.conf',
      }
    }
    if ($localproxy) {
      if ($http_proxy or $https_proxy) {
        class { 'localproxy':
          digest_user => hiera('proxy_digest_user'),
          digest_password => hiera('proxy_digest_password'),
        }
      }
    }
  }
} # end os_base::proxy

# == Class: os_base::repository
# setup de repositórios diversos
class os_base::repositories {
  if ('RedHat' == $::osfamily) {
    # EPEL é necessário para instalar alguns pacotes
    file { 'epel.repo':
      path => "/etc/yum.repos.d/epel.repo",
      source =>'/vagrant/downloads/epel.repo',
    }
  }
} # end os_base::repositories

# == Class: os_base::puppet_modules
# instala módulos puppet necessários
class os_base::puppet_modules {
  exec {'puppet_modules_install':
    path => '/bin',
    command => '/vagrant/scripts/install_modules.sh',
    cwd => "/vagrant/scripts/",
    onlyif => [ "test ! -d /vagrant/modules/apache",
                "test ! -d /vagrant/modules/mysql"],
    environment => $exec_environment
  }
} # end os_base::puppet_modules

# == Class: os_base
# configurações básicas do servidor
class os_base {
  include os_base::repositories
  include os_base::proxy
  include os_base::puppet_modules
  include os_base::firewall
} # end os_base

node default {
  include os_base
  include rapps::devel
} # end node default
