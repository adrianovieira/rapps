# RAPPS - Provisionamento

## Instrodução

É possível obter uma amostra de funcionamento da aplicação, ou até mesmo contribuir com o desenvolvimento usando uma máquina virtual local.

Para tanto, estão disponíveis aqui neste repositório ```scripts``` e arquivos de parâmetros para provisionamento automatizado em ```Vagrant```+```VirtualBox```+```Puppet```.

  - ```Vagrant```: *"a tool for building complete development environments".* - http://vagrantup.com/
  - ```VirtualBox```: *"a general-purpose full virtualization product for x86 and AMD64/Intel64 hardware".* - http://virtualbox.org/
  - ```Puppet```: *"a configuration management tool".* - http://puppetlabs.com/

## A fazer

- Finalizar esta documentação
- Finalizar provisionamento para arquivos de configurações usarem os dados ```hiera```

## Requisitos

As ferramentas ```Vagrant``` e ```VirtualBox``` instaladas em seu ambiente de teste/desenvolvimento.

Também será necessário haver acesso transparente à internet.

### Passos iniciais

Após haver instalado as ferramentas acima, no diretório ```doc/provision/puppet```, execute:

1. ```mkdir modules```
1. ```mkdir environments/dev```
1. ```vagrant up node-EL7-puppet --no-provision```
1. ```vagrant ssh node-EL7-puppet```
1. ```sudo su -```
1. ```cd /vagrant/scripts```
1. ```sh install_modules.sh```
1. ```exit```
1. ```exit```

  Isso é necessário para que os módulos sejam "baixados" para a sua máquina de trabalho (é isso mesmo, o seu host - evitando que tenha que instalar, desnecessariamente, o puppet na sua estação de trabalho).
  Estes passos não precisarão ser repetidos (se você não apagar o diretório ```modules``` criado no passo 1 acima).

### Provisionamento de demonstração/desenvolvimento

Após os passos acima será possível realizar provisionamentos subsequentes e acessar a aplicação a partir de seu navegador.

1. Crie o arquivo hiera (```environments/hieradata/local_hiera.yaml ```) com o seguinte conteúdo*******:

  ```yaml
  ---
  rapps_http_service_name: dev.rapps.puppet
  rapps_http_service_name_aliases: dev.rapps
  rapps_path_deploy: /var/www/html/rapps
  rapps_cache: /var/tmp/rapps
  rapps_git_repository: 'https://gitlab.com/adrianovieira/rapps.git'
  rapps_git_repository_version: 0.0.1
  rapps_db_user_name: dbapps_username
  rapps_db_user_pass: dbapps_userpass
  rapps_db_host: localhost
  rapps_db_name: db_rapps

  ```

  ****Altere este conteúdo conforme sua necessidade.***

  Onde:

    - `rapps_http_service_name` e `rapps_http_service_name_aliases`: nomes para acesso ao serviço (ex: `http://dev.rapps.puppet` ou `http://dev.rapps`)
    - `rapps_path_deploy`: diretório para deploy da aplicação
    - `rapps_cache`: diretório para *cache* da aplicação
    - `rapps_git_repository`: repositório da aplicação
    - `rapps_git_repository_version`: versão (*branch* ou *tag*) da aplicação para *deploy*
    - `rapps_db_user_name`: nome do usuário do banco de dados
    - `rapps_db_user_pass`: senha do usuário do banco de dados
    - `rapps_db_host`: servidor do banco de dados
    - `rapps_db_name`: nome do banco de dados


1. Rotina de provisionamento

  Considerando que o passo ```1``` foi realizado com sucesso, essa rotina de provisionamento poderá ser repetida sem a necessidade d´aquele passo ser executado novamente - se o o conteúdo do diretório ```modules``` não for apagado.

  Comandos básicos a serem usados:

  1. criar a máquina virtual (VM): ```vagrant up```
  1. provisionamento da VM e aplicação: ```vagrant provision```
  1. excluir VM: ```vagrant destroy```
