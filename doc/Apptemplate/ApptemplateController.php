<?php
/**
 * Controller para a aplicação Apptemplates
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @package Apptemplate
 * @subpackage base
 * @license @see LICENCE
 */
namespace Apptemplate;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ApptemplateController implements ControllerProviderInterface {

  public function connect(Application $app)
  {
      // cria novo módulo controller
      $controllers = $app['controllers_factory'];

      // lista serviços disponíveis
      $controllers->get('/', 'Apptemplate\ApptemplateController::index' )->bind('apptemplate');
      /*
      $controllers->get('/{service_id}', 'Apptemplate\ApptemplateController::getApptemplateById' );
      $controllers->put('/{service_id}', 'Apptemplate\ApptemplateController::updateApptemplateById');
      $controllers->post('/', 'Apptemplate\ApptemplateController::addApptemplate');
      $controllers->delete('/{service_id}', 'Apptemplate\ApptemplateController::deleteApptemplateById');
      */

      return $controllers;
  }

  public function index(Request $request, Application $app) {

    return $app['twig']->render('Apptemplate/views/Apptemplate-main.twig');
  }

  /*
  public function addApptemplate(Request $request, Application $app) {
    // continua se autenticado, senão: throw AccessDeniedHttpException
    \Apps\Security\SecurityController::isAuthenticated($app);

    return $app['twig']->render('Apptemplate/views/itservice-main.twig');
  }
  */

}
