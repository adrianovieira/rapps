# RAPPS - Instalação

## Dependências

Instale os requisitos de PHP (>= 5.4).

```bash
yum install php php-cli php-common php-xml
```

Instale dependências de *módulos* para o Silex

  1. Instale o gerenciador de pacotes [*Composer*](<https://getcomposer.org/>)

    ```
    https://getcomposer.org/download/
    ```

  1. Instale dependências de *módulos* para o Silex

    ```bash
    ln -s composer-dist.json composer.json

    composer install
    ```
  1. Instale dependências para automatização de tarefas

    É necessário estar instalado o [*NodeJS*](<http://nodejs.org>)

    ```bash
    cd scripts
    ```

    ```
    npm install
    ```

  1. Excecute tarefas pre-definidas via *grunt*

    - Lista de tarefas pre-definidas

    ```bash
    grunt --help
    ```

    - Execute as tarefas padrão definidas

    ```bash
    grunt
    ```

  1. Adapte o arquivo de configurações da aplicação

    ```bash
    cp config/config-dist.ini.php config/config.ini.php
    ```

    Edite e altere o arquivo ```config/config.ini.php``` de acordo com o ambiente.

  1. Crie certificado SSL para o serviço HTTP (opcional)

    Substituirá os arquivos na pasta `setup` (na raiz do projeto)

    ```bash
    openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -keyout setup/rapps_setup_sample_selfsigned_ssl_http_server.key -out setup/rapps_setup_sample_selfsigned_ssl_http_server.cert
    ```
