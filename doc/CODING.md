# Contribuições

Algumas instruções e dicas para contribuições no desenvolvimento.

# Inclusão de Aplicativos (Controller)

Pode ser necessário que controllers específicos precisem ser configurados para serem carregados na inicialização da Aplicação. Sendo este o caso, é preciso adicionar o diretório de sua aplicação no arquivo ```composer-dist.json (composer.json)``` (*composer autoloading*).

- Possível sintoma:

  ```php
  InvalidArgumentException in ControllerResolver.php line 153:
  Class "cms\Front\Controller\HomeController" does not exist.
  ```

- Edite o aquivo e atualize o campo ***autoload***

  ```json
  "autoload": {
      "psr-0": {"Apps": "src/"}
  }
  ```
  Após alterar o arquivo é necessário executar o ```composer dump-autoload``` para que o *autoload* possa ser atualizado.

- Referências:
  - *Composer Autoloading* <https://getcomposer.org/doc/01-basic-usage.md#autoloading>  
  - PSR-0 <http://www.php-fig.org/psr/psr-0/> (a especificação PSR-4 requer sintaxe de namespace como Apps\\Modulo\\Classe)

## Dicas

1. Para mudar comportamento do twig quanto às tags do template

    ```php
    $app->before(function() use ($app){
        $app['twig']->setLexer( new Twig_Lexer($app['twig'], [
            'tag_comment'   => ['{#', '#}'],
            'tag_block'     => ['{%', '%}'],
            'tag_variable'  => ['{{', '}}'],
            'interpolation' => ['#{', '}'],
        ]));
    });
    ```

1. AngularJS Template tag/symbol

    Default: ```{{```  ```}}```

    Example: <https://docs.angularjs.org/api/ng/provider/$interpolateProvider>

    - Change:

   ```javascript
   $interpolateProvider.startSymbol('{|').endSymbol('|}');
   ```

1. Alternativa: usar mascaramento de string no twig (**recomendado**)

    ```javascript
    {{ '{{var_angular}}' }}
    ```
    onde: ```{{``` => externos=twig, internos=angular;

## Apptemplate

- `Apptemplate`: substituir pelo ID da aplicação, ex: `Itservice` (***Camelcase***)
- `apptemplate`: substituir pelo ID da aplicação, ex: `itservice` (***camelcase***)
