module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jslint: {
      tema: {
        src: [
          'js/*.js'
        ],
        directives: {
          browser: true,
          predef: [
            '$', 'angular'
          ]
        },
        options: {
          junit: 'out/tema-junit.xml'
        }
      }
    },

    jshint: {
        all: [ 'Gruntfile.js', 'js/*.js' ],
        options: {
          //jshintrc: '.jshintrc'
          "curly": true,
          "eqeqeq": true,
          "immed": true,
          "latedef": true,
          "newcap": true,
          "noarg": true,
          "sub": true,
          "undef": true,
          "boss": true,
          "eqnull": true,
          "node": true,
          "es5": false
      }
    },

    bower: {
      cleanup: {
        options: {
          cleanTargetDir: true,
          cleanBowerDir: true,
          install: false,
          copy: false
        }
      },

      cleanupNodeModules: {
        options: {
          targetDir: 'node_modules',
          cleanTargetDir: true,
          cleanBowerDir: false,
          verbose: false,
          install: false,
          copy: false
        }
      },

      install: {
        options: {
          production : true,
          install: true,
          verbose: true,
          copy: false,
          cleanTargetDir: false,
          cleanBowerDir: false,
          bowerOptions: {}
        }
      }
    },

    uglify: {
        options: {
          mangle: true,
          //beautify: true
        },

        build: {
          src: "js/*.js",
          dest: "js/rapps.min.js"
        }
    },

    log: {
          foo: [1, 2, 3],
          bar: 'hello world',
          baz: false
    },

    copy: {
        main: {
            src: 'src/*',
            dest: 'dest/',
        },
    }

  }); // end grunt.initConfig

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jslint');
  grunt.loadNpmTasks('grunt-preen');
  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task(s).
  grunt.registerTask('default', ['bower:install', 'preen']);

  grunt.registerTask('cleanupAll', 'Clean up devDependencies', function() {
    grunt.log.writeln(this.name + ': cleaning...');
    grunt.task.run(['bower:cleanup', 'bower:cleanupNodeModules']);

  });

};
