FROM mysql:8.0

LABEL maintainer Adriano Vieira <adriano.svieira at gmail.com>

# database init
COPY setup/sql/ /docker-entrypoint-initdb.d/
