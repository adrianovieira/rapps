<?php
/**
 * Programa principal para a aplicação App Skeleton
 *
 * Esse é o programa principal e deverá ser usado inclusive para
 * para chamadas e construções das diversas funcionalidades.
 *
 * Apresentará a tela principal como padrão
 *
 * @file
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @modified Adriano Vieira <adriano.svieira at gmail.com>
 * @package App Skeleton
 * @license @see LICENCE
 */

ini_set('display_errors', 0);

require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';

require __DIR__.'/../src/app-controllers.php';

$app->run();
