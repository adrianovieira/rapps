/* jshint -W097 */
/* exported apps, elemento, app, PageController, ComponentesController  */
/* globals angular, $ */

(function () {
  'use strict';
  angular
      .module('rapps')
      .controller('AdminUsersController', AdminUsersController);

  AdminUsersController.$inject = ['$scope', '$http', '$log', '$location'];

  // @TODO
  //  - verficar como ativar/desativar modo debug angular
  //  - realizar varlidações antes de enviar os dados
  function AdminUsersController($scope, $http, $log, $location) {
    var closure = this; // avoiding contextual changes

    var dado_original = [];
    var dado_atual = [];

    closure.base_path = $location.absUrl();
    closure.message = [];
    closure.message.text = null;
    closure.message.success = null;
    closure.pagination = false;
    closure.auth_method = ['Database', 'LDAP']; //['Database', 'LDAP', '3dAuth']
    closure.no_yes = ['Não', 'Sim'];

    // interface
    closure.getUsers = getUsers; // LIST: user(s)
    closure.getUserById = getUserById; // GET user
    closure.enableUserById = enableUserById; //enable/disable user
    closure.userPutPost = userPutPost;
    closure.deleteUser = deleteUser; // DELETE: user(s)
    //closure.putUsers = updateUser; // PUT|UPDATE: user(s)
    //closure.addUser = addUser; // POST|ADD: user(s)

    function deleteUser(username) {
      $http.delete( closure.base_path+username)
        .then(
          function(response) {
            $log.debug('RAPPS AdminUsersController: success deleteUser response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.users.splice(findFirstObjItem(closure.users,'username',username),1);
            }
          },
          function(error){
            $log.error('RAPPS AdminUsersController: error deleteUser', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    }; // end deleteUser

    function updateUser(username, user) {
      $log.debug('RAPPS AdminUsersController: success updateUser [', user , ']');
      var data = '{"username":"'+user['username']+
                '","name":"'+user['name']+
                '","email":"'+user['email']+
                '","auth_method":"'+user['auth_method']+
                '"}';
      var headers = "{'Content-Type': 'application/json'}";

      $http.put( closure.base_path+username, data, headers )
        .then(
          function(response) {
            $log.debug('RAPPS AdminUsersController: success updateUser response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.users[findFirstObjItem(closure.users,'username',username)] = user;
            }
          },
          function(error){
            $log.error('RAPPS AdminUsersController: error updateUser', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    } // end updateUser

    function addUser(user) {
      $log.debug('RAPPS AdminUsersController: success userPutPost addUser [', user , ']');
      var data = '{"username":"'+user['username']+
                '","name":"'+user['name']+
                '","email":"'+user['email']+
                '","auth_method":"'+user['auth_method']+
                '"}';
      var headers = "{'Content-Type': 'application/json'}";

      $http.post( closure.base_path, data, headers )
        .then(
          function(response) {
            $log.debug('RAPPS AdminUsersController: success addUser response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.users.push(user);
            }
          },
          function(error){
            $log.error('RAPPS AdminUsersController: error addUser', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    } // end addUser

    function userPutPost(action_adduser, username='') {
      if (action_adduser) {
        return addUser(closure.user);
      } else {
        $log.debug('RAPPS AdminUsersController: success userPutPost updateUser [', closure.user, ']');
        return updateUser(username, closure.user);
      }
    }

    // obtem  usuário cadastrado
    function getUser(username='') {
      closure.user = null;
      $http.get( closure.base_path+username)
          .then(
            function(response) {
              $log.debug('RAPPS AdminUsersController: success getUserById response ', response);
              closure.user = response.data;
              closure.user.enabled = (closure.user.is_enabled==1)?"Sim":"Não";
              $log.debug('RAPPS AdminUsersController: success getUserById', closure.user);
            },
            function(error){
              $log.error('RAPPS AdminUsersController: error getUserById', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getUserById

    // obtem lista de todos usuários cadastrados
    function getUsers(username='') {
      closure.users = null;
      $http.get( closure.base_path+'dashboard')
          .then(
            function(response) {
              $log.debug('RAPPS AdminUsersController: success getUsers response ', response);
              closure.users = response.data;
              $log.debug('RAPPS AdminUsersController: success getUsers', closure.users);
            },
            function(error){
              $log.error('RAPPS AdminUsersController: error getUsers', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getUsers

    // obtem lista de todos usuários cadastrados
    function getUserById(username) {
      closure.user = null;
      $http.get( closure.base_path+username)
          .then(
            function(response) {
              $log.debug('RAPPS AdminUsersController: success getUserById response ', response);
              closure.user = response.data;
              closure.user.enabled = (closure.user.is_enabled==1)?"Sim":"Não";
              $log.debug('RAPPS AdminUsersController: success getUserById', closure.user);
            },
            function(error){
              $log.error('RAPPS AdminUsersController: error getUserById', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getUserById

    function enableUserById(username, enabled) {
      enabled = (enabled==1?0:1);
      $http.put( closure.base_path+'enable/'+username+'/'+enabled)
        .then(
          function(response) {
            $log.debug('RAPPS AdminUsersController: success enableUserById response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.users[findFirstObjItem(closure.users,'username',username)]['is_enabled'] = enabled;
            }
          },
          function(error){
            $log.error('RAPPS AdminUsersController: error enableUserById', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    }; // end enableUserById

    // busca primeira ocorrencia do "item" de "key" no "obj"
    // @TODO
    //  - migrar como serviço
    function findFirstObjItem(obj, key, item) {
      var index = -1;
      for (var idx = 0; idx < obj.length; idx++) {
        if (obj[idx][key] == item) {
          index = idx;
          break;
        }
      }
      return index;
    }; // end findFirstObjItem

  } // end AdminUsersController

}());
