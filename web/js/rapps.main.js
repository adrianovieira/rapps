/* jshint -W097 */
/* exported apps, elemento, app, PageController, ComponentesController  */
/* globals angular, $ */

(function () {
  'use strict';
  angular
      .module('rapps', [])
      .controller('MainController', MainController);

  MainController.$inject = ['$scope', '$http', '$location', '$log'];

  function MainController($scope, $http, $location, $log) {
    var closure = this; // avoiding contextual changes

    // base template status variables (default values)
    closure.has_messages = false;
    closure.has_notifications = false;
    closure.has_tasks = false;
    closure.has_user_profile = false;
    closure.has_user_body = false;
    closure.has_user_activities = false;
    closure.has_user_stats = false;
    closure.has_user_settings = false;
    closure.has_user_photo = false;
    closure.has_search_field = false;

    // interfase
    closure.enableHasMessages = enableHasMessages;

    //

    function enableHasMessages() {
      closure.has_messages = true;
    }

  }

}());
