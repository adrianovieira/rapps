/* rapps-admin-aplicativos-valid-logotipo-file.directive.js */

/**
* @desc order diretiva que valida arquivo de logotipo
* @example <div rapps-admin-aplicativos-valid-logotipo-file></div>
*/

(function () {
  'use strict';

  angular
      .module('rapps')
      .directive('rappsAdminAplicativosValidLogotipoFile', AdminAplicativosValidLogotipoFile);

  function AdminAplicativosValidLogotipoFile() {
      return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
          // change event is fired when file is selected
          el.bind('change',function(){
                  scope.$apply(function(){
                      ngModel.$setViewValue(el.val());
                      ngModel.$render();
                  });
          });
        }
      };
  }; // end AdminAplicativosValidLogotipoFile

}()); // end closure
