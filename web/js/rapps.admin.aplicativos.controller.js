/* jshint -W097 */
/* exported apps, elemento, app, PageController, ComponentesController  */
/* globals angular, $ */

(function () {
  'use strict';
  angular
      .module('rapps')
      .controller('AdminAplicativosController', AdminAplicativosController);

  AdminAplicativosController.$inject = ['$scope', '$http', '$log', '$location'];

  // @TODO
  //  - verficar como ativar/desativar modo debug angular
  //  - realizar varlidações antes de enviar os dados
  function AdminAplicativosController($scope, $http, $log, $location) {
    var closure = this; // avoiding contextual changes

    var dado_original = [];
    var dado_atual = [];

    closure.base_path = $location.absUrl();
    closure.message = [];
    closure.message.text = null;
    closure.message.success = null;

    // interface
    closure.getAppsDataLista = getAppsDataLista;
    closure.getAppsDataById = getAppsDataById;
    closure.getAppsUsersById = getAppsUsersById;
    closure.sendAppsDataForm = sendAppsDataForm;
    closure.initAppsAddForm = initAppsAddForm;
    closure.habilitarAppsById = habilitarAppsById;
    closure.publicarAppsById = publicarAppsById;
    closure.deleteAppsDataById = deleteAppsDataById;

    // obtem lista de todos aplicativos cadastrados
    function getAppsDataLista() {
      $http.get( closure.base_path+'lista')
          .then(
            function(response) {
              $log.debug('RAPPS AdminAplicativosController: success getAppsDataLista response ', response);
              closure.aplicativos = response.data;
              $log.debug('RAPPS AdminAplicativosController: success getAppsDataLista aplicativos', closure.aplicativos);
            },
            function(error){
              $log.error('RAPPS AdminAplicativosController: error getAppsDataLista', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getAppsDataLista

    // obtem dados de aplicativo
    function getAppsDataById(apps_id) {
      closure.aplicativo = [];
      closure.message.text = null;
      closure.message.success = null;
      $http.get( closure.base_path+apps_id)
          .then(
            function(response) {
              $log.debug('RAPPS AdminAplicativosController: success getAppsDataById response', response);
              closure.aplicativo.data = response.data;
              closure.aplicativo.app_id = response.data.app_id;
              closure.aplicativo.route = response.data.route;
              closure.aplicativo.name = response.data.name;
              closure.aplicativo.title = response.data.title;
              closure.aplicativo.logotype = response.data.logotype;
              closure.form_method = 'put';
              dado_original['app_id'] = closure.aplicativo.app_id;
              dado_original['route'] = closure.aplicativo.route;
              dado_original['name'] = closure.aplicativo.name;
              dado_original['title'] = closure.aplicativo.title;
              dado_original['logotype'] = closure.aplicativo.logotype;
              closure.aplicativo.readonly_id = true;
            },
            function(error){
              $log.error('RAPPS AdminAplicativosController: error getAppsDataById', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getAppsDataById

    // obtem lista de usuários de aplicativo
    function getAppsUsersById(apps_id) {
      closure.message.text = null;
      closure.message.success = null;
      closure.aplicativos_users = null;

      $log.debug('RAPPS AdminAplicativosController: getAppsUsersById url', closure.base_path+apps_id+'/users');

      $http.get( closure.base_path+apps_id+'/users')
          .then(
            function(response) {
              $log.debug('RAPPS AdminAplicativosController: success getAppsUsersById response ', response);
              closure.aplicativos_users = response.data.users;
              $log.debug('RAPPS AdminAplicativosController: success getAppsUsersById aplicativos_users', closure.aplicativos_users);
            },
            function(error){
              $log.error('RAPPS AdminAplicativosController: response error getAppsUsersById', error);
              closure.message.success = false;
              closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
            }
          );
    }; // end getAppsUsersById

    function sendAppsDataForm() {
      var url = (closure.form_method == 'put') ?
                  closure.base_path + '/' + closure.aplicativo.data.app_id : closure.base_path;
      closure.message = [];

      $log.debug('RAPPS AdminAplicativosController: logging [form_method]: ', closure.form_method);
      $log.debug('RAPPS AdminAplicativosController: logging [apps.id]: ', closure.aplicativo.data.app_id);
      $log.debug('RAPPS AdminAplicativosController: logging [url]: ', url);

      dado_atual['app_id'] = closure.aplicativo.data.app_id;
      dado_atual['route'] = closure.aplicativo.data.route;
      dado_atual['name'] = closure.aplicativo.data.name;
      dado_atual['title'] = closure.aplicativo.data.title;
      dado_atual['logotype'] = closure.aplicativo.data.logotype;

      if (closure.form_method == 'put') {
        // avança somente se dados foram alterados
        // (!angular.equals(dado_original, dado_atual)) // why this don't work
        if ((dado_original['route'] != dado_atual['route']) ||
            (dado_original['name'] != dado_atual['name'])   ||
            (dado_original['title'] != dado_atual['title'])  ||
            (dado_original['logotype'] != dado_atual['logotype'])
           ) {
          if (!isValidAdminAplicativo()) {
            $log.debug('RAPPS AdminAplicativosController: dados inválidos, não enviados');
            return false;
          }
        } else {
          $log.debug('RAPPS AdminAplicativosController: dados sem alterações, não enviados');
          return false;
        }
      };
      $log.debug('RAPPS AdminAplicativosController: dados a enviar',dado_atual);
      var aplicativo = JSON.parse('{"app_id":"'+dado_atual['app_id']+
              '","route":"'+dado_atual['route']+
              '","name":"'+dado_atual['name']+
              '","title":"'+dado_atual['title']+
              '","logotype":"'+dado_atual['logotype']+
              '"}');

      $http({
        url: (closure.form_method == 'put') ? closure.base_path + closure.aplicativo.data.app_id : closure.base_path,
        method: closure.form_method,
        data: '{"app_id":"'+dado_atual['app_id']+
                '","route":"'+dado_atual['route']+
                '","name":"'+dado_atual['name']+
                '","title":"'+dado_atual['title']+
                '","logotype":"'+dado_atual['logotype']+
                '"}',
        headers: {'Content-Type': 'application/json'}
      })
        .then(function(response) {
          $log.debug('RAPPS AdminAplicativosController: success sendAppsDataForm response', response);

          closure.message.text = response.data.message.text;
          closure.message.success = response.data.message.success;
          if (closure.form_method == 'post') { // se inclusão com sucesso
            closure.aplicativos.push(aplicativo);
          } else if (closure.form_method == 'put') {
            dado_original['app_id'] = closure.aplicativo.data.app_id;
            dado_original['route'] = closure.aplicativo.data.route;
            dado_original['name'] = closure.aplicativo.data.name;
            dado_original['title'] = closure.aplicativo.data.title;
            dado_original['logotype'] = closure.aplicativo.data.logotype;
            closure.aplicativos[findFirstObjItem(closure.aplicativos,'app_id',dado_atual['app_id'])] = aplicativo;
          }
        },
        function(error) {
          $log.error('RAPPS AdminAplicativosController: error sendAppsDataForm', error);
          closure.message.success = false;
          closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
        }
      );
    }; // end sendAppsDataForm

    // realizar varlidações antes de enviar os dados
    function isValidAdminAplicativo() {
      var resp = false;
      var msg = null;
      closure.message.success = false;
      closure.message.text = msg;

      resp = true; // por enquanto não valida dados

      return resp;
    }; //end isValidAdminAplicativo

    // busca primeira ocorrencia do "item" de "key" no "obj"
    // @TODO
    //  - migrar como serviço
    function findFirstObjItem(obj, key, item) {
      var index = -1;
      for (var idx = 0; idx < obj.length; idx++) {
        if (obj[idx][key] == item) {
          index = idx;
          break;
        }
      }
      return index;
    }; // end findFirstObjItem

    function initAppsAddForm() {
      cleanAppsDataForm();
      closure.form_method = 'post';
    }; // end initAppsAddForm

    function cleanAppsDataForm() {
      closure.aplicativo.data.app_id = null;
      closure.aplicativo.data.route = null;
      closure.aplicativo.data.name = null;
      closure.aplicativo.data.title = null;
      closure.aplicativo.data.logotype = null;

      closure.aplicativo.readonly_id = false;
      closure.message.success = null;
      closure.message.text = null;
    }; // end cleanAppsDataForm

    function habilitarAppsById(apps_id, habilitar) {
      habilitar = (habilitar==1?0:1);
      $http.put( closure.base_path+'habilitar/'+apps_id+'/'+habilitar)
        .then(
          function(response) {
            $log.debug('RAPPS AdminAplicativosController: success habilitarAppsById response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.aplicativos[findFirstObjItem(closure.aplicativos,'app_id',apps_id)]['is_active'] = habilitar;
            }
          },
          function(error){
            $log.error('RAPPS AdminAplicativosController: error habilitarAppsById', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    }; // end habilitarAppsById

    function publicarAppsById(apps_id, roles) {
      var publicar = (roles == "ROLE_GUEST"?0:1);
      $http.put( closure.base_path+'publicar/'+apps_id+'/'+publicar)
        .then(
          function(response) {
            $log.debug('RAPPS AdminAplicativosController: success publicarAppsById response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.aplicativos[findFirstObjItem(closure.aplicativos,'app_id',apps_id)]['roles'] = (roles == "ROLE_GUEST"?0:'ROLE_GUEST');
            }
          },
          function(error){
            $log.error('RAPPS AdminAplicativosController: error publicarAppsById', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    }; // end habilitarAppsById

    function deleteAppsDataById(apps_id) {
      if (!apps_id) {
        return false;
      }
      $http.delete( closure.base_path+apps_id)
        .then(
          function(response) {
            $log.debug('RAPPS AdminAplicativosController: success deleteAppsDataById response ', response);
            closure.message.text = response.data.message.text;
            closure.message.success = response.data.message.success;
            if (closure.message.success) {
              closure.aplicativos.splice(findFirstObjItem(closure.aplicativos,'app_id',apps_id),1);
            }
          },
          function(error){
            $log.error('RAPPS AdminAplicativosController: error deleteAppsDataById', error);
            closure.message.success = false;
            closure.message.text = error.data.message.text+" ("+error.statusText + "/"+error.status+")";
          }
        );
    }; // end deleteAppsDataById

  } // end AdminAplicativosController

}());
